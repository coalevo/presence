/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.presence.model.PresenceStatus;
import net.coalevo.presence.model.PresenceStatusType;
import net.coalevo.presence.model.PresenceStatusTypes;

/**
 * This class implements a base {@link PresenceStatus}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceStatusImpl
    implements PresenceStatus {

  protected String m_Description;
  protected PresenceStatusType m_Type;
  protected PresenceStatusType m_LastType;
  protected boolean m_Updated;
  protected boolean m_WasAdAvail = false;

  public PresenceStatusImpl(PresenceStatusType type, String desc) {
    m_Type = type;
    m_Description = desc;
  }//constructor

  public String getDescription() {
    return m_Description;
  }//getDescription

  public synchronized void setDescription(String str) {
    if (m_Description == str ||
        (m_Description != null && m_Description.equals(str))) {
      return;
    }
    m_Description = str;
    m_Updated = true;
  }//setDescription

  public synchronized void setType(PresenceStatusType type) {
    if (type == null) {
      return;
    }
    if (type.equals(m_Type)) {
      return;
    }
    m_LastType = m_Type;
    m_Type = type;
    m_Updated = true;
  }//setPresenceStatusType

  public PresenceStatusType getType() {
    return m_Type;
  }//getType

  public boolean isType(PresenceStatusType type) {
    return m_Type.equals(type);
  }//isType

  public boolean isAvailable() {
    return (
           m_Type.equals(PresenceStatusTypes.Available)
        || m_Type.equals(PresenceStatusTypes.AdvertisedAvailable)
        || m_Type.equals(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)
        || m_Type.equals(PresenceStatusTypes.TemporarilyUnavailable)
    );
  }//isAvailable

  public boolean isUnavailable() {
    return (m_Type.equals(PresenceStatusTypes.Unavailable));
  }//isUnavailable

  public boolean isUpdated() {
    return m_Updated;
  }//isUpdated

  public boolean wasPublicUpdate() {
    return ((m_LastType.equals(PresenceStatusTypes.AdvertisedAvailable) &&
        m_Type.equals(PresenceStatusTypes.AdvertisedTemporarilyUnavailable))
       || (m_Type.equals(PresenceStatusTypes.AdvertisedAvailable) &&
        m_LastType.equals(PresenceStatusTypes.AdvertisedTemporarilyUnavailable))
    );
  }//wasPublicUpdate

  public synchronized void setUpdated(boolean b) {
    m_Updated = b;
  }//setUpdated

  public boolean equals(Object o) {
    if (o == null || getClass() != o.getClass()) return false;

    PresenceStatusImpl that = (PresenceStatusImpl) o;
    if (m_Description != null ? !m_Description.equals(that.m_Description) : that.m_Description != null)
      return false;
    if (m_Type != null ? !m_Type.equals(that.m_Type) : that.m_Type != null)
      return false;

    return true;
  }//equals

  public int hashCode() {
    int result;
    result = (m_Description != null ? m_Description.hashCode() : 0);
    result = 31 * result + (m_Type != null ? m_Type.hashCode() : 0);
    return result;
  }//hashCode

}//class PresenceStatusImpl
