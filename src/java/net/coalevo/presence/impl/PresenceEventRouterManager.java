/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.presence.model.PresenceEventRouter;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implements a white board model router manager for
 * presences.
 * <p>
 * Will pick up
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceEventRouterManager {

  private Marker m_LogMarker = MarkerFactory.getMarker(PresenceEventRouterManager.class.getName());
  private BundleContext m_BundleContext;
  private Map<String, PresenceEventRouter> m_PresenceEventRouters;

  public PresenceEventRouterManager() {
    m_PresenceEventRouters = new ConcurrentHashMap<String, PresenceEventRouter>();
  }//PresenceEventRouterManager

  /**
   * Activates this <tt>PresenceEventRouterManager</tt>.
   * The logic will automatically register all {@link PresenceEventRouter}
   * class objects, whether registered before or after the activation
   * (i.e. white board model implementation).
   *
   * @param bc the <tt>BundleContext</tt>.
   */
  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare listener
    PresenceEventRouterListener pl = new PresenceEventRouterListener();
    //prepare the filter
    String filter = "(objectclass=" + PresenceEventRouter.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(pl, filter);
      //ensure that already registered ShellService instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        pl.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
    }
  }//activate

  /**
   * Deactivates this <tt>ShellServiceManagerImpl</tt>.
   * The logic will remove the listener and release all
   * references.
   */
  public void deactivate() {

    //null out the references
    m_PresenceEventRouters.clear();

    m_PresenceEventRouters = null;
    m_BundleContext = null;
  }//deactivate

  public boolean register(PresenceEventRouter er) {
    String dom = er.getDomain();
    if (m_PresenceEventRouters.containsKey(dom)) {
      return false;
    } else {
      m_PresenceEventRouters.put(dom, er);
      Activator.log().info(m_LogMarker, "Registered PresenceEventRouter for domain " + dom);
      return true;
    }
  }//register

  public boolean unregister(String dom) {
    if (!m_PresenceEventRouters.containsKey(dom)) {
      return false;
    } else {
      m_PresenceEventRouters.remove(dom);
      Activator.log().info(m_LogMarker, "Unregistered PresenceEventRouter for domain " + dom);
      return true;
    }
  }//unregister

  public PresenceEventRouter get(String dom) {
    Object o = m_PresenceEventRouters.get(dom);
    if (o != null) {
      return (PresenceEventRouter) o;
    } else {
      throw new NoSuchElementException(dom);
    }
  }//get

  public boolean isAvailable(String dom) {
    return m_PresenceEventRouters.containsKey(dom);
  }//isAvailable

  public Iterator listAvailable() {
    return m_PresenceEventRouters.keySet().iterator();
  }//listAvailable


  private class PresenceEventRouterListener
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof PresenceEventRouter)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:registered:Reference not a PresenceEventRouter instance.");
          } else {
            register((PresenceEventRouter) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof PresenceEventRouter)) {
            Activator.log().error(m_LogMarker, "ServiceListener:serviceChanged:unregistering:Reference not a PresenceEventRouter instance.");
          } else {
            unregister(((PresenceEventRouter) o).getDomain());
          }
          break;
      }
    }
  }//inner class PresenceEventRouterListener


}//class PresenceEventRouterManager
