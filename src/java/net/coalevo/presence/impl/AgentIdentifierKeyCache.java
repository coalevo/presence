/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.AgentIdentifierInstanceCache;
import net.coalevo.foundation.util.LRUCacheMap;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a cache for keys of agent identifiers to be used for
 * update and query statement parameters.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AgentIdentifierKeyCache {

  private Marker m_LogMarker = MarkerFactory.getMarker(AgentIdentifierKeyCache.class.getName());
  private LRUCacheMap<AgentIdentifier, String> m_Cache = new LRUCacheMap<AgentIdentifier, String>(50);
  private LRUCacheMap<String, AgentIdentifier> m_ReverseCache = new LRUCacheMap<String, AgentIdentifier>(50);
  private PresenceStore m_Store;
  private Map<String, String> m_Params;
  private AgentIdentifierInstanceCache m_AIDCache;


  public AgentIdentifierKeyCache(PresenceStore ps) {
    m_Store = ps;
    m_Params = new HashMap<String, String>();
    m_AIDCache = ps.getAgentIdentifierCache();
  }//AgentIdentifierKeyCache

  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  /**
   * Sets maximum number of cached instances.
   *
   * @param size the maximum cached instances.
   */
  public void setCacheSize(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
      m_ReverseCache.setCeiling(size);
    }
  }//setCacheSize

  public synchronized String getKeyFor(AgentIdentifier aid) {
    String key = null;
    //1. test cache
    Object o = null;
    //synchronized (m_Cache) {
    o = m_Cache.get(aid);
    //}
    if (o != null) {
      key = (String) o;
    } else {
      //2.test database
      try {
        key = getDatabaseKey(aid);
        //add to cache
        //synchronized(m_Cache) {
        m_Cache.put(aid, key);
        m_ReverseCache.put(key, aid);
        //}
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getKeyFor()", ex);
      }
    }
    return key;
  }//getKeyFor

  public synchronized AgentIdentifier getAgentIdentifierFor(String key) {
    AgentIdentifier aid = null;
    //1. test cache
    Object o = null;
    //synchronized (m_Cache) {
    o = m_ReverseCache.get(key);
    //}
    if (o != null) {
      aid = (AgentIdentifier) o;
    } else {
      //2.test database
      try {
        aid = getDatabaseAgentIdentifier(key);
        //add to cache
        //synchronized(m_Cache) {
        m_ReverseCache.put(key, aid);
        //}
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "getAgentIdentifierFor()", ex);
      }
    }
    return aid;
  }//getAgentIdentifier

  private final String getDatabaseKey(AgentIdentifier aid) throws Exception {
    String key = null;
    LibraryConnection lc = null;
    ResultSet rs = null;
    synchronized (m_Params) {
      m_Params.put("aid", aid.getIdentifier());
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getAgentIdentifierKey", m_Params);
        if (rs.next()) {
          key = rs.getString(1);
        } else {
          //3. create the key
          lc.executeUpdate("createAgentIdentifierKey", m_Params);
          rs = lc.executeQuery("getAgentIdentifierKey", m_Params);
          if (rs.next()) {
            key = rs.getString(1);
          } else {
            throw new Exception("Key for agentidentifier was not created.");
          }
        }
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
        m_Params.remove("aid");
      }
    }
    return key;
  }//getDatabaseKey

  private final AgentIdentifier getDatabaseAgentIdentifier(String key) throws Exception {
    AgentIdentifier aid = null;
    LibraryConnection lc = null;
    ResultSet rs = null;
    synchronized (m_Params) {
      m_Params.put("agent_key", key);
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getAgentIdentifierForKey", m_Params);
        if (rs.next()) {
          aid = m_AIDCache.get(rs.getString(1));
        }
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
        m_Params.remove("agent_key");
      }
    }
    return aid;
  }//getDatabaseAgentIdentifier

  public void clear() {
    m_Cache.clear(false);
    m_ReverseCache.clear(false);
  }//clear

}//class AgentIdentifierKeyCache
