/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.presence.service.PresenceConfiguration;
import net.coalevo.presence.service.PresenceService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a <tt>BundleActivator</tt> implementation for
 * the userdata bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static LogProxy c_Log;
  private static Marker c_LogMarker;

  private static ServiceMediator c_Services;
  private static Messages c_BundleMessages;
  private static PresenceService c_PresenceService;
  private static PresenceEventRouterManager c_PERMgr;
  private static BundleConfiguration c_BundleConfiguration;
  private static PresenceStore c_PresenceStore;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Log
              c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
              c_Log = new LogProxy();
              c_Log.activate(bundleContext);

              //2. Services
              c_Services = new ServiceMediator();
              c_Services.activate(bundleContext);

              //3. Bundle Messages
              c_BundleMessages =
                  c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                      .getBundleMessages(bundleContext.getBundle());

              //4. Bundle Configuration
              c_BundleConfiguration = new BundleConfiguration(PresenceConfiguration.class.getName());
              c_BundleConfiguration.activate(bundleContext);
              c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

              //5. Presence Store
              c_PresenceStore = new PresenceStore();
              c_PresenceStore.activate(bundleContext);
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.store"));

              //6. Presence Service
              c_PresenceService = new PresenceServiceImpl(c_PresenceStore);
              if (!c_PresenceService.activate(bundleContext)) {
                log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "PresenceService"));
                return;
              }
              String[] classes = {PresenceService.class.getName(), Maintainable.class.getName()};
              bundleContext.registerService(
                  classes,
                  c_PresenceService,
                  null);
              log().debug(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "PresenceService"));

              //7. Presence Routing (for distribution)
              c_PERMgr = new PresenceEventRouterManager();
              c_PERMgr.activate(bundleContext);
              c_Services.setPresenceEventRouterManager(c_PERMgr);

            } catch (Exception ex) {
              log().error("", ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext)
      throws Exception {

    //wait start
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    if (c_PresenceService != null) {
      c_PresenceService.deactivate();
      c_PresenceService = null;
    }
    if (c_PERMgr != null) {
      c_PERMgr.deactivate();
      c_PERMgr = null;
    }
    if (c_PresenceStore != null) {
      c_PresenceStore.deactivate();
      c_PresenceStore = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if(c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
