/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.*;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;

/**
 * Provides a base implementation for presence related
 * event management.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
abstract class PresenceEventManager {

  private GenericObjectPool.Config m_PoolConfig;
  protected GenericObjectPool m_PresenceNotifyPool;
  protected GenericObjectPool m_StatusUpdateNotifyPool;
  protected GenericObjectPool m_ResourceUpdateNotifyPool;
  protected GenericObjectPool m_PresenceProbePool;
  protected GenericObjectPool m_PresenceUnregisterPool;
  protected ServiceMediator m_Services = Activator.getServices();

  public PresenceEventManager() {
    //5. Prepare pools
    m_PoolConfig = new GenericObjectPool.Config();
    m_PoolConfig.maxActive = 25;
    m_PoolConfig.maxIdle = 5;
    m_PoolConfig.minIdle = 5;
    //block and wait
    m_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    m_PoolConfig.maxWait = -1;
    //test on borrow
    m_PoolConfig.testOnBorrow = true;
    m_PresenceNotifyPool = new GenericObjectPool(new PresenceNotifyFactory(), m_PoolConfig);
    m_StatusUpdateNotifyPool = new GenericObjectPool(new StatusUpdateNotifyFactory(), m_PoolConfig);
    m_ResourceUpdateNotifyPool = new GenericObjectPool(new ResourceUpdateNotifyFactory(), m_PoolConfig);
    m_PresenceProbePool = new GenericObjectPool(new PresenceProbeFactory(), m_PoolConfig);
    m_PresenceUnregisterPool = new GenericObjectPool(new PresenceUnregisterFactory(), m_PoolConfig);
  }//constructor


  public abstract PresenceListener getPresenceListener(AgentIdentifier aid);

  public abstract Iterator getPublicPresenceListeners();

  public abstract Presence getRegistered(AgentIdentifier aid);

  public abstract Iterator getPresenceRegistrationListeners();

  class PresenceNotifyFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new PresenceNotify(m_PresenceNotifyPool);
    }//makeObject

    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof PresenceNotify) {
        ((PresenceNotify) o).reset();
      }
    }//passivateObject

  }//inner class PresenceNotifyFactory

  class StatusUpdateNotifyFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new StatusUpdateNotify(m_StatusUpdateNotifyPool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof StatusUpdateNotify) {
        ((StatusUpdateNotify) o).reset();
      }
    }//passivateObject


  }//inner class StatusUpdateNotifyFactory

  class ResourceUpdateNotifyFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new ResourceUpdateNotify(m_ResourceUpdateNotifyPool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof ResourceUpdateNotify) {
        ((ResourceUpdateNotify) o).reset();
      }
    }//passivateObject

  }//inner class ResourceUpdateNotifyFactory

  class PresenceProbeFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new PresenceProbe(m_PresenceProbePool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof PresenceProbe) {
        ((PresenceProbe) o).reset();
      }
    }//passivateObject

  }//inner class PresenceProbeFactory

  class PresenceUnregisterFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new PresenceUnregister(m_PresenceUnregisterPool);
    }//makeObject


    public void passivateObject(Object o)
        throws Exception {
      if (o instanceof PresenceUnregister) {
        ((PresenceUnregister) o).reset();
      }
    }//passivateObject

  }//inner class PresenceUnregisterFactory

  class PresenceNotify
      implements Runnable {

    private Marker m_LogMarker = MarkerFactory.getMarker(PresenceNotify.class.getName());

    private GenericObjectPool m_Pool;
    private PresenceImpl m_Presence;
    private boolean m_Present;

    public PresenceNotify(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setPresence(PresenceImpl p) {
      m_Presence = p;
      m_Present = p.isPresent();
    }//setPresence

    public void reset() {
      m_Presence = null;
      m_Present = false;
    }//reset

    public void run() {
      try {
        try {
          m_Presence.lockEvents();
          if (m_Presence.getStatus().isType(PresenceStatusTypes.AdvertisedAvailable)) {
            notifyPublic();
          }
          notifySubscribed();
        } finally {
          m_Presence.unlockEvents();
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "PresenceNotify::run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "PresenceNotify::run()", e);
        }
      }

    }//run

    private void notifyPublic() {
      //local
      for (Iterator iter = getPublicPresenceListeners(); iter.hasNext();) {
        PresenceListener l = (PresenceListener) iter.next();
        Presence p = l.getOwner();
        if (m_Presence.allowsOutbound(p.getAgentIdentifier()) //outbound blocks
            && p.allowsInbound(m_Presence.getAgentIdentifier()) //inbound blocks
            && l.isActive()) {
          if (m_Present) {
            l.becamePresent(m_Presence.getProxy());
          } else {
            l.becameAbsent(m_Presence.getProxy());
          }
        }
      }
      //route it onto the network
      for (Iterator iter = m_Services.getPresenceEventRouterManager().listAvailable(); iter.hasNext();) {
        PresenceEventRouter per = (PresenceEventRouter) iter.next();
        if (m_Present) {
          per.routeBecamePresent(m_Presence.getProxy());
        } else {
          per.routeBecameAbsent(m_Presence.getProxy());
        }
      }
    }//notifyPublic

    private void notifySubscribed() {
      for (Iterator iter = m_Presence.getSubscriptionsFrom().iterator(); iter.hasNext();) {
        AgentIdentifier aid = (AgentIdentifier) iter.next();
        if (aid.isLocal()) {
          PresenceListener l = getPresenceListener(aid);
          if (l != null && l.isActive()) {
            if (m_Present) {
              l.becamePresent(m_Presence.getProxy());
            } else {
              l.becameAbsent(m_Presence.getProxy());
            }
          }
        } else {
          //route it onto the network
          String dom = m_Presence.getAgentIdentifier().getNode();
          PresenceEventRouterManager perm = m_Services.getPresenceEventRouterManager();
          if (perm.isAvailable(dom)) {
            PresenceEventRouter per = perm.get(dom);
            if (m_Present) {
              per.routeBecamePresent(m_Presence.getProxy());
            } else {
              per.routeBecameAbsent(m_Presence.getProxy());
            }
          }
        }
      }
    }//notifySubscribed

  }//PresenceNotify

  class StatusUpdateNotify
      implements Runnable {

    private GenericObjectPool m_Pool;
    private PresenceImpl m_Presence;
    private Marker m_LogMarker = MarkerFactory.getMarker(StatusUpdateNotify.class.getName());

    public StatusUpdateNotify(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setPresence(PresenceImpl p) {
      m_Presence = p;
    }//setPresence

    public void reset() {
      m_Presence = null;
    }//reset

    public void run() {
      try {
        try {
          m_Presence.lockEvents();
           if (((PresenceStatusImpl) m_Presence.getStatus()).wasPublicUpdate()) {
            notifyPublic(true, true);
          } else {
            if (m_Presence.getStatus().isType(PresenceStatusTypes.AdvertisedAvailable)
                ||m_Presence.getStatus().isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)) {
              notifyPublic(false, true);
            } else {
              notifyPublic(false,false);
            }
          }
          notifySubscribed();
        } finally {
          m_Presence.unlockEvents();
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "run()", e);
        }
      }
    }//run

    private void notifyPublic(boolean update, boolean presence) {
      //local
      for (Iterator iter = getPublicPresenceListeners(); iter.hasNext();) {
        PresenceListener l = (PresenceListener) iter.next();
        Presence p = l.getOwner();
        if (m_Presence.allowsOutbound(p.getAgentIdentifier()) //outbound blocks
            && p.allowsInbound(m_Presence.getAgentIdentifier()) //inbound blocks
            && l.isActive()) {
          if (update) {
            l.statusUpdated(m_Presence.getProxy());
          } else {
            if(presence) {
              l.becamePresent(m_Presence.getProxy());
            } else {
              l.becameAbsent(m_Presence.getProxy());
            }
          }
        }
      }
      //route it onto the network
      for (Iterator iter = m_Services.getPresenceEventRouterManager().listAvailable(); iter.hasNext();) {
        PresenceEventRouter per = (PresenceEventRouter) iter.next();
        if (update) {
          per.routeStatusUpdated(m_Presence.getProxy());
        } else {
          if(presence) {
            per.routeBecamePresent(m_Presence.getProxy());
          } else {
            per.routeBecameAbsent(m_Presence.getProxy());
          }
        }
      }
    }//notifyPublic

    private void notifySubscribed() {
      for (Iterator iter = m_Presence.getSubscriptionsFrom().iterator(); iter.hasNext();) {
        AgentIdentifier aid = (AgentIdentifier) iter.next();
        if (aid.isLocal()) {
          //handle local
          PresenceListener l = getPresenceListener(aid);
          if (l != null && l.isActive()) {
            l.statusUpdated(m_Presence.getProxy());
          }
        } else {
          //route it onto the network
          String dom = m_Presence.getAgentIdentifier().getNode();
          PresenceEventRouterManager perm = m_Services.getPresenceEventRouterManager();
          if (perm.isAvailable(dom)) {
            PresenceEventRouter per = perm.get(dom);
            per.routeStatusUpdated(m_Presence.getProxy(), aid);
          }
        }
      }
    }//notifySubscribed

  }//StatusUpdateNotify

  class ResourceUpdateNotify
      implements Runnable {

    private GenericObjectPool m_Pool;
    private PresenceImpl m_Presence;
    private Marker m_LogMarker = MarkerFactory.getMarker(ResourceUpdateNotify.class.getName());

    public ResourceUpdateNotify(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setPresence(PresenceImpl p) {
      m_Presence = p;
    }//setPresence

    public void reset() {
      m_Presence = null;
    }//reset

    public void run() {
      try {
        try {
          m_Presence.lockEvents();
          if (m_Presence.getStatus().isType(PresenceStatusTypes.AdvertisedAvailable)) {
            notifyPublic();
          }
          notifySubscribed();
        } finally {
          m_Presence.unlockEvents();
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "run()", e);
        }
      }
    }//run

    private void notifyPublic() {
      //local
      for (Iterator iter = getPublicPresenceListeners(); iter.hasNext();) {
        PresenceListener l = (PresenceListener) iter.next();
        Presence p = l.getOwner();
        if (m_Presence.allowsOutbound(p.getAgentIdentifier()) //outbound blocks
            && p.allowsInbound(m_Presence.getAgentIdentifier()) //inbound blocks
            && l.isActive()) {
          l.resourcesUpdated(m_Presence.getProxy());
        }
      }
      //route it onto the network
      for (Iterator iter = m_Services.getPresenceEventRouterManager().listAvailable(); iter.hasNext();) {
        PresenceEventRouter per = (PresenceEventRouter) iter.next();
        per.routeResourcesUpdated(m_Presence.getProxy());
      }
    }//notifyPublic

    private void notifySubscribed() {
      for (Iterator iter = m_Presence.getSubscriptionsFrom().iterator(); iter.hasNext();) {
        AgentIdentifier aid = (AgentIdentifier) iter.next();
        if (aid.isLocal()) {
          //handle local
          PresenceListener l = getPresenceListener(aid);
          if (l != null && l.isActive()) {
            l.resourcesUpdated(m_Presence.getProxy());
          }
        } else {
          //route it onto the network
          String dom = m_Presence.getAgentIdentifier().getNode();
          PresenceEventRouterManager perm = m_Services.getPresenceEventRouterManager();
          if (perm.isAvailable(dom)) {
            PresenceEventRouter per = perm.get(dom);
            per.routeResourcesUpdated(m_Presence.getProxy(), aid);
          }
        }
      }
    }//notifySubscribed

  }//ResourceUpdateNotify

  class PresenceProbe
      implements Runnable {

    private GenericObjectPool m_Pool;
    private Presence m_Presence;
    private Object m_ToProbe;
    private Marker m_LogMarker = MarkerFactory.getMarker(PresenceProbe.class.getName());


    public PresenceProbe(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setPresence(Presence p) {
      m_Presence = p;
    }//setPresence

    public void setToProbe(Object o) {
      m_ToProbe = o;
    }//setToProbe

    public void reset() {
      m_Presence = null;
    }//reset

    public void run() {
      try {
        Activator.log().debug(m_LogMarker, "run()::o=" + m_Presence.getAgentIdentifier().toString());
        if (m_ToProbe instanceof AgentIdentifier) {
          //single one
          probe((AgentIdentifier) m_ToProbe);
        } else if (m_ToProbe instanceof AgentIdentifierList) {
          //all loop
          AgentIdentifierList ail = (AgentIdentifierList) m_ToProbe;
          for (Iterator iter = ail.iterator(); iter.hasNext();) {
            AgentIdentifier aid = (AgentIdentifier) iter.next();
            probe(aid);
          }
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "run()", e);
        }
      }
    }//run

    private void probe(AgentIdentifier aid) {
      if (aid.isLocal()) {
        Presence p = getRegistered(aid);
        if (p != null) {
          getPresenceListener(
              m_Presence.getAgentIdentifier())
              .receivedPresence(p.getProxy());
        }
      } else {
        String dom = aid.getNode();
        PresenceEventRouterManager perm = m_Services.getPresenceEventRouterManager();
        if (perm.isAvailable(dom)) {
          PresenceEventRouter per = perm.get(dom);
          per.routeProbeRequest(m_Presence.getProxy(), aid);
        }
      }

    }//probe

  }//PresenceProbe

  class PresenceUnregister implements Runnable {

    private GenericObjectPool m_Pool;
    private Presence m_Presence;
    private Marker m_LogMarker = MarkerFactory.getMarker(PresenceUnregister.class.getName());

    public PresenceUnregister(GenericObjectPool pool) {
      m_Pool = pool;
    }//constructor

    public void setPresence(Presence p) {
      m_Presence = p;
    }//setPresence

    public void reset() {
      m_Presence = null;
    }//reset

    public void run() {
      try {
        for (Iterator iter = getPresenceRegistrationListeners(); iter.hasNext();) {
          PresenceRegistrationListener prl = (PresenceRegistrationListener) iter.next();
          prl.unregistered(m_Presence);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "run()", ex);
      } finally {
        try {
          m_Pool.returnObject(this);
        } catch (Exception e) {
          Activator.log().error(m_LogMarker, "run()", e);
        }
      }
    }//run

  }//PresenceUnregister

  /*
  TODO: Routing of incoming presence events to local listeners
        We need to iterate all routers and dequeue all events
        to distribute them locally.

  class IncomingPresenceEventHandler
      implements Runnable {

    private AtomicBoolean m_Run = new AtomicBoolean(true);

    public void run() {
      do {
        try {
          PresenceEvent pe = m_PresenceEventRouter.dequeueNextEvent();
          AgentIdentifier originator = pe.getPresence().getAgentIdentifier();
          AgentIdentifier target = pe.getTarget();
          if (pe.isProbe()) {
            //if online and not blocked
            PresenceListener tpl = getPresenceListener(target);
            Presence tplowner = tpl.getOwner();
            if (tpl != null && tpl.isActive() && tplowner.allowsOutbound(originator)) {
              if (originator.isLocal()) {
                PresenceListener opl = getPresenceListener(originator);
                if (opl != null && opl.isActive()) {
                  opl.receivedPresence(tplowner.getProxy());
                }
              }
            }
            continue;
          }
          if (pe.isPublic()) {
            notifyPublic(pe);
          } else {
            notifySubscribed(pe);
          }
        } catch (Exception ex) {
          Activator.log().error("IncomingPresenceHandler::run()", ex);
        }
      } while (m_Run.get());
    }//run

    private void notifyPublic(PresenceEvent pe) {
      PresenceProxy presence = pe.getPresence();
      for (Iterator iter = getPublicPresenceListeners(); iter.hasNext();) {
        PresenceListener l = (PresenceListener) iter.next();
        Presence p = l.getOwner();
        if (p.allowsInbound(presence.getAgentIdentifier()) //inbound blocks
            && l.isActive()) {
          if (pe.isBecamePresent()) {
            l.becamePresent(presence);
          } else if (pe.isBecameAbsent()) {
            l.becameAbsent(presence);
          } else if (pe.isStatusUpdate()) {
            l.statusUpdated(presence);
          }
        }
      }
    }//notifyPublic


    private void notifySubscribed(PresenceEvent pe) {
      PresenceProxy presence = pe.getPresence();
      PresenceListener l = getPresenceListener(pe.getTarget());
      if (l != null && l.isActive()) {
        if (pe.isBecamePresent()) {
          l.becamePresent(presence);
        } else if (pe.isBecameAbsent()) {
          l.becameAbsent(presence);
        } else if (pe.isStatusUpdate()) {
          l.statusUpdated(presence);
        }
      }
    }//notifySubscribed

  }//IncomingHandler
*/

}//class PresenceEventManager
