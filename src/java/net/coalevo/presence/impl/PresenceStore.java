/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.presence.service.PresenceConfiguration;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.security.service.SecurityService;
import net.coalevo.userdata.service.UserdataService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Provides an implementation of a presence store.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceStore
    implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(PresenceStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;
  private boolean m_New = false;
  private Messages m_BundleMessages = null;


  //Concurrency handling backup/restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_RestoreLatch;
  private CountDownLatch m_LeaseLatch;

  //Caches
  private AgentIdentifierKeyCache m_KeyCache;
  private AgentIdentifierInstanceCache m_AgentIdentifierCache;

  private boolean m_FriendshipMode = false;

  public PresenceStore() {
  }//constructor

  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();

    //1. Configure from persistent configuration
    String ds = "embedded";
    int cpoolsize = 2;
    int aidcachesize = 50;

    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

    //Retrieve config
    try {
      ds = mtd.getString(PresenceConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception", "attribute", PresenceConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(PresenceConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception", "attribute", PresenceConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(PresenceConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception", "attribute", PresenceConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      m_FriendshipMode = mtd.getBoolean(PresenceConfiguration.FRIENDSHIP_MODE_KEY).booleanValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception", "attribute", PresenceConfiguration.FRIENDSHIP_MODE_KEY),
          ex
      );
    }
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //2. Prepare SQL library
    try {
      m_SQLLibrary =
          new Library(PresenceStore.class, "net/coalevo/presence/impl/presencestore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }

    DataSourceService dss = Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);

    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.datasource"),
          nse
      );
      return false;
    }
    //prepare store
    prepareDataSource();

    Activator.log().info(m_LogMarker,
        m_BundleMessages.get("PresenceStore.database.info", "source", m_DataSource.toString())
    );
    //Prepare Caches (need store)
    m_AgentIdentifierCache = new AgentIdentifierInstanceCache(aidcachesize);
    m_KeyCache = new AgentIdentifierKeyCache(this);

    //add handler config updates
    cm.addUpdateHandler(this);

    return true;
  }//activate


  public synchronized boolean deactivate() {

    //1. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(m_LogMarker, "deactivate()", e);
      }
    }

    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public boolean isNew() {
    return m_New;
  }//isNew


  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        m_New = true;
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private synchronized boolean createSchema(LibraryConnection lc) {
    try {
      lc = leaseConnection();
      lc.executeCreate("createPresenceSchema");
      lc.executeCreate("createKeyTable");
      lc.executeCreate("createSubscriptionsFromTable");
      lc.executeCreate("createSubscriptionsToTable");
      lc.executeCreate("createRequestedSubscriptionsTable");
      lc.executeCreate("createInboundBlocksTable");
      lc.executeCreate("createOutboundBlocksTable");
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "createDatabase()", ex);
    } finally {
      releaseConnection(lc);
    }
    return false;
  }//createDatabase

  public void update(MetaTypeDictionary mtd) {
    //1. Configure from persistent configuration
    int cpoolsize = 2;
    int aidcachesize = 50;

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(PresenceConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception",
              "attribute",
              PresenceConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      aidcachesize = mtd.getInteger(PresenceConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception",
              "attribute",
              PresenceConfiguration.AGENTIDENTIFIERS_CACHESIZE_KEY),
          ex
      );
    }
    try {
      m_FriendshipMode = mtd.getBoolean(PresenceConfiguration.FRIENDSHIP_MODE_KEY).booleanValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("PresenceStore.activation.configexception", "attribute", PresenceConfiguration.FRIENDSHIP_MODE_KEY),
          ex
      );
    }
    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);
    m_AgentIdentifierCache.setCeiling(aidcachesize);
  }//update

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    if (m_ConnectionPool == null) {
      return null;
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      if (m_ConnectionPool == null) return;
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(m_LogMarker, "releaseConnection()", e);
    }
  }//releaseConnection

  public AgentIdentifierKeyCache getKeyCache() {
    return m_KeyCache;
  }//getKeyCache

  public AgentIdentifierInstanceCache getAgentIdentifierCache() {
    return m_AgentIdentifierCache;
  }//getAgentIdentifierCache

  public boolean isFriendshipMode() {
    return m_FriendshipMode;
  }//isFriendshipMode

  public void doBackup(Agent caller, File f, String tag)
      throws SecurityException, BackupException {
  }//doBackup

  public void doRestore(Agent caller, File f, String tag)
      throws SecurityException, RestoreException {
  }//doRestore

  //TODO: Probably there should be a corresponding ServiceDataService, to check for services.
  //TODO: Currently implemented to ignore agents that have the role "Service" (hardcoded)
  public void maintain(ServiceAgentProxy agent) throws MaintenanceException {
    //Check against user data
    UserdataService uds = Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
    if (uds == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.service", "service", "UserdataService"));
      return;
    }
    SecurityService ss = agent.getSecurityService();
    if (ss == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.service", "service", "SecurityService"));
      return;
    }

    //Database maintainance
    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();

    LibraryConnection lc = null;
    try {

      lc = leaseConnection();
      //1. purge registrations not available from userdata
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.unregister"));
      rs = lc.executeQuery("listRegisteredAgents", null);
      while (rs.next()) {
        String aid = rs.getString(1);
        AgentIdentifier a_id = new AgentIdentifier(aid);
        if (!uds.isUserdataAvailable(agent.getAuthenticPeer(), a_id)) {
          //Check that it isn't a service by role
          //HACK: See CSB-9
          Set roles = ss.getAgentRolesByName(agent.getAuthenticPeer(), a_id);
          boolean isService = false;
          for (Object o : roles) {
            if (o.equals("Service")) {
              isService = true;
              break;
            }
          }
          //If it is a service, ignore it
          if (isService) {
            continue;
          }
          //Purge if no userdata available
          params.put("aid", aid);
          try {
            lc.executeUpdate("deleteAgentIdentifierKey", params);
            Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.unregistered", "aid", params.get("agent_id")));
          } catch (Exception ex) {
            Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.unregister", "aid", params.get("agent_id")));
          }
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.store"), ex);
    } finally {
      SqlUtils.close(rs);
      releaseConnection(lc);
    }
    //clear caches
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.store.caches"));
    m_AgentIdentifierCache.clear();
    m_KeyCache.clear();

  }//doMaintenance


  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate", null);
      } catch (SQLException ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class PresenceStore
