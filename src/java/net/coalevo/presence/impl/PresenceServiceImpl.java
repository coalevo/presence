/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.presence.model.*;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Provides an implementation of {@link PresenceService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceServiceImpl
    extends BaseService
    implements PresenceService, Maintainable {

  private Marker m_LogMarker = MarkerFactory.getMarker(PresenceServiceImpl.class.getName());
  private ServiceMediator m_Services;
  private Messages m_BundleMessages;
  //Security Context
  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private AtomicBoolean m_Active = new AtomicBoolean(false);

  private PresenceStore m_Store;
  private AgentIdentifierInstanceCache m_AIDCache;
  private AgentIdentifierKeyCache m_KeyCache;
  protected EventHandler m_EventHandler;
  protected Map<AgentIdentifier, PresenceServiceListener> m_PresenceServiceListeners;
  protected Map<AgentIdentifier, Presence> m_Registered;
  protected Map<AgentIdentifier, CountDownLatch> m_SessionEndedPresences;
  protected List<PresenceListener> m_PublicPresenceListeners;
  protected List<PresenceRegistrationListener> m_PresenceRegistrationListeners;

  public PresenceServiceImpl(PresenceStore ps) {
    super(PresenceService.class.getName(), ACTIONS);
    m_Store = ps;
    m_EventHandler = new EventHandler();
    m_PresenceServiceListeners = new ConcurrentHashMap<AgentIdentifier, PresenceServiceListener>();
    m_Registered = new ConcurrentHashMap<AgentIdentifier, Presence>();
    m_SessionEndedPresences = new ConcurrentHashMap<AgentIdentifier, CountDownLatch>();
    m_PublicPresenceListeners = new ArrayList<PresenceListener>();
    m_PresenceRegistrationListeners = new ArrayList<PresenceRegistrationListener>();

    //Caches from Store
    m_KeyCache = m_Store.getKeyCache();
    m_AIDCache = m_Store.getAgentIdentifierCache();
  }//constructor

  public boolean activate(BundleContext bc) {
    if (m_Active.getAndSet(true)) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.active", "service", "PresenceService"));
      return false;
    }

    m_Services = Activator.getServices();
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/PresenceService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    return true;
  }//activate

  public boolean deactivate() {
    if (m_Active.getAndSet(false)) {

      if (m_PresenceServiceListeners != null) {
        m_PresenceServiceListeners.clear();
        m_PresenceServiceListeners = null;
      }
      if (m_Registered != null) {
        m_Registered.clear();
        m_Registered = null;
      }
      if (m_PublicPresenceListeners != null) {
        synchronized (m_PublicPresenceListeners) {
          m_PublicPresenceListeners.clear();
        }
        m_PublicPresenceListeners = null;
      }
      if (m_PolicyProxy != null) {
        m_PolicyProxy.deactivate();
        m_PolicyProxy = null;
      }
      if (m_ServiceAgentProxy != null) {
        m_ServiceAgentProxy.deactivate();
        m_ServiceAgentProxy = null;
      }
      if (m_KeyCache != null) {
        m_KeyCache.clear();
        m_KeyCache = null;
      }
      if (m_Store != null) {
        m_Store.deactivate();
        m_Store = null;
      }
      m_EventHandler = null;
      m_BundleMessages = null;
      return true;
    }
    return false;
  }//deactivate

  public Presence register(Agent agent, PresenceServiceListener psl) {
    m_ServiceAgentProxy.getSecurityService().checkAuthentic(agent);
    final AgentIdentifier aid = agent.getAgentIdentifier();

    //hook up to session means the agent is authenticated as well
    PresenceImpl p = null;

    if (agent instanceof UserAgent) {
      //System.err.println("PresenceService::register()::Have agent with session");
      Session s = ((UserAgent) agent).getSession();
      if (s != null && s.isValid()) {
        if (m_SessionEndedPresences.containsKey(aid)) {
          CountDownLatch l = (CountDownLatch) m_SessionEndedPresences.get(aid);
          try {
            l.await();
          } catch (InterruptedException e) {
            Activator.log().error(m_LogMarker, "register()", e);
          }
        }
        p = new PresenceImpl(agent, m_Store, this);
        //System.err.println("PresenceService::register()::Created presence");

        psl.setOwner(p);
        //add listener
        m_PresenceServiceListeners.put(aid, psl);
        //register
        m_Registered.put(aid, p);
        //cache identifier
        m_AIDCache.put(aid);
        s.setAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE, p);
        s.addSessionListener(
            new SessionListener() {
              public void invalidated(Session s) {
                final AgentIdentifier aid = s.getAgent().getAgentIdentifier();
                //1. Check if the session ended without unregistering the presence
                if (PresenceServiceImpl.this.isRegistered(aid)) {
                  //2. Create Wait Latch and add it to the collection
                  CountDownLatch l = new CountDownLatch(1);
                  PresenceServiceImpl.this.m_SessionEndedPresences.put(aid, l);
                  try {
                    //3. The presence can be obtained
                    Presence p =
                        (Presence) PresenceServiceImpl.this.m_Registered.get(aid);
                    //4. End presence
                    if (p.isPresent()) {
                      p.end(); //end presence if not ended
                    }
                    //5. Unregister
                    m_PresenceServiceListeners.remove(aid);
                    m_Registered.remove(aid);
                    removePublicPresenceListener(p);
                    m_EventHandler.notifyAllOfUnregister(p);
                  } finally {
                    l.countDown();
                    PresenceServiceImpl.this.m_SessionEndedPresences.remove(aid);
                  }
                  Activator.log().debug(m_LogMarker, this.toString() + " ended presence by session invalidation (" + aid.toString() + ").");

                }

              }//invalidated
            }//anonymous SessionListener
        );
        return p;
      }
    }
    //System.err.println("PresenceService::register()::Have agent, no session or session invalid");
    p = new PresenceImpl(agent, m_Store, this);
    //System.err.println("PresenceService::register()::Created presence");

    psl.setOwner(p);
    //add listener
    m_PresenceServiceListeners.put(aid, psl);
    //register
    m_Registered.put(aid, p);
    //cache identifier
    m_AIDCache.put(aid);
    return p;
  }//register

  public void unregister(Presence p) {
    AgentIdentifier aid = p.getAgentIdentifier();
    if (!isRegistered(aid)) {
      throw new IllegalStateException("Presence " + p.toString() + " is not registered with this service instance.");
    }
    if (p.isPresent()) {
      throw new IllegalStateException("Presence " + p.toString() + " is still present.");
    }
    //do cleanup
    m_PresenceServiceListeners.remove(aid);
    m_Registered.remove(aid);
    removePublicPresenceListener(p);
    m_EventHandler.notifyAllOfUnregister(p);
  }//unregister

  public void registerPublicPresenceListener(PresenceListener pl) {
    ensureRegistered(pl.getOwner());
    synchronized (m_PublicPresenceListeners) {
      m_PublicPresenceListeners.add(pl);
    }
  }//registerPublicPresenceListener

  public void unregisterPublicPresenceListener(PresenceListener pl) {
    synchronized (m_PublicPresenceListeners) {
      m_PublicPresenceListeners.remove(pl);
    }
  }//unregisterPublicPresenceListener

  private void removePublicPresenceListener(Presence p) {
    synchronized (m_PublicPresenceListeners) {
      for (Iterator iterator = m_PublicPresenceListeners.listIterator(); iterator.hasNext();) {
        PresenceListener presenceListener = (PresenceListener) iterator.next();
        if (presenceListener.getOwner().equals(p)) {
          iterator.remove();
          break;
        }
      }
    }
  }//removePublicPresenceListener

  public void addPresenceRegistrationListener(Agent caller, PresenceRegistrationListener prl)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), ADD_REGISTRATION_LISTENER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "addPresenceRegistrationListener()", ex);
    }
    synchronized (m_PresenceRegistrationListeners) {
      m_PresenceRegistrationListeners.add(prl);
    }
  }//addPresenceRegistrationListener

  public void removePresenceRegistrationListener(Agent caller, PresenceRegistrationListener prl)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), REMOVE_REGISTRATION_LISTENER);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "RemovePresenceRegistrationListener()", ex);
    }
    synchronized (m_PresenceRegistrationListeners) {
      m_PresenceRegistrationListeners.remove(prl);
    }
  }//removePresenceRegistrationListener

  /*
    Subscription Requests
  */
  public boolean requestSubscriptionTo(final Presence me, final AgentIdentifier to) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    //check that me is not already subscribed
    if (mep.isSubscribedTo(to)) {
      return false;
    }
    //check if to is registered, requires online handling and notification
    if (isRegistered(to)) {
      PresenceImpl top = (PresenceImpl) m_Registered.get(to);
      //block check
      if (!top.allowsInbound(meid)) {
        return false;
      }
      //Add:
      AgentIdentifierList reqsub = top.getRequestedSubscriptions();
      if (!reqsub.contains(meid)) {
        reqsub.add(meid);
        //Notify if present
        if (top.isPresent()) {
          m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(
              m_ServiceAgentProxy.getAuthenticPeer(),
              new Runnable() {
                public void run() {
                  m_EventHandler.getPresenceListener(to).requestedSubscription(me.getProxy());
                }//run
              }//Runnable
          );//execute
        }
        return true;
      }
    } else {  //2. to is not registered, requires offline handling without notification
      //1. if to is not valid or me is blocked
      if (isAgentValid(to) && !isInboundBlocked(to, meid)) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("aid1", m_KeyCache.getKeyFor(to));
        params.put("aid2", m_KeyCache.getKeyFor(meid));
        LibraryConnection lc = null;
        ResultSet rs = null;
        try {
          lc = m_Store.leaseConnection();
          rs = lc.executeQuery("checkPendingRequest", params);
          rs.next();
          if (rs.getInt(1) == 0) {
            lc.executeUpdate("addRequestedSubscription", params);
          }
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "requestSubscriptionTo()", ex);
        } finally {
          SqlUtils.close(rs);
          m_Store.releaseConnection(lc);
        }
        return true;
      }
    }
    return false;
  }//requestSubscriptionTo

  protected boolean isAgentValid(AgentIdentifier aid) {
    LibraryConnection lc = null;
    ResultSet rs = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("aid", aid.getIdentifier());
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getAgentIdentifierKey", params);
      if (rs.next()) {
        return true;
      } else {
        return false;
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "isAgentValid()", ex);
      return false;
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//isAgentValid

  public List<AgentIdentifier> listPendingRequests(Presence me) {
    List<AgentIdentifier> aid = new ArrayList<AgentIdentifier>();
    Map<String, String> params = new HashMap<String, String>();
    params.put("aid2", m_KeyCache.getKeyFor(me.getAgentIdentifier()));
    LibraryConnection lc = null;
    ResultSet rs = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listPendingRequests", params);
      while (rs.next()) {
        aid.add(m_AIDCache.get(rs.getString(1)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "listPendingRequests()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return aid;
  }//listPendingRequests

  public void allowSubscription(final Presence me, final AgentIdentifier other) {
    final AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    if (!mep.isRequestedSubscription(other)) {
      throw new IllegalStateException("Not a requested subscription.");
    }
    //add subscription other agent to me
    mep.getSubscriptionsFrom().add(other);
    //remove the request
    mep.getRequestedSubscriptions().remove(other);
    if(m_Store.isFriendshipMode()) {
      mep.getSubscriptionsTo().add(other);
    }
    //check if to is registered, requires online handling and notification
    if (isRegistered(other)) {
      final PresenceImpl fromp = (PresenceImpl) m_Registered.get(other);
      //add subscription to me to the other agent
      fromp.getSubscriptionsTo().add(meid);
      if(m_Store.isFriendshipMode()) {
        fromp.getSubscriptionsFrom().add(meid);
      }
      //notify the allowed subscriber
      if (fromp.isPresent()) {
        //subscription
        m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(
            m_ServiceAgentProxy.getAuthenticPeer(),
            new Runnable() {
              public void run() {
                PresenceServiceListener psl =
                    (PresenceServiceListener) m_PresenceServiceListeners.get(other);
                psl.subscriptionAllowed(meid);
                //presence
                PresenceListener pl = m_EventHandler.getPresenceListener(other);
                pl.becamePresent(me.getProxy());
                if(m_Store.isFriendshipMode()) {
                  //run probe
                  probePresence(fromp, me.getAgentIdentifier());
                }
              }//run
            }//Runnable
        );//execute
      }
    } else {   //to is not registered, requires some offline handling without notification
      Map<String, String> params = new HashMap<String, String>();
      params.put("aid1", m_KeyCache.getKeyFor(other));
      params.put("aid2", m_KeyCache.getKeyFor(meid));
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("addSubscriptionTo", params);
        if(m_Store.isFriendshipMode()) {
          lc.executeUpdate("addSubscriptionFrom", params);
          params.put("aid1", m_KeyCache.getKeyFor(meid));
          params.put("aid2", m_KeyCache.getKeyFor(other));
          lc.executeUpdate("addSubscriptionTo",params);
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "allowSubscription()", ex);
      } finally {
        m_Store.releaseConnection(lc);
      }
    }
  }//allowSubscription

  public void denySubscription(Presence me, final AgentIdentifier from, final String reason) {
    final AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    if (!mep.isRequestedSubscription(from)) {
      throw new IllegalStateException("Not a requested subscription.");
    }
    mep.getRequestedSubscriptions().remove(from);
    //check if to is registered, requires online handling and notification
    if (isRegistered(from)) {
      PresenceImpl fromp = (PresenceImpl) m_Registered.get(from);
      if (fromp.isPresent()) {
        m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(
            m_ServiceAgentProxy.getAuthenticPeer(),
            new Runnable() {
              public void run() {
                PresenceServiceListener psl =
                    (PresenceServiceListener) m_PresenceServiceListeners.get(from);
                psl.subscriptionDenied(meid, (reason != null) ? reason : "");
              }//run
            }//Runnable
        );//execute
      }
    }
  }//denySubscription

  public void unsubscribeFrom(Presence me, AgentIdentifier other, boolean notify) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    //sanity check
    if (!mep.isSubscribedTo(other)) {
      return;
    }
    // remove my subscription to the other
    mep.getSubscriptionsTo().remove(other);
    doUnsubscribe(meid, other, notify);
    if(m_Store.isFriendshipMode()) {
      mep.getSubscriptionsFrom().remove(other);
      this.doCancelSubscription(mep,meid,other,false);
    }
  }//unsubscribeFrom

  public void cancelSubscriptionOf(Presence me, AgentIdentifier other, boolean notify) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    //sanity check
    if (!mep.isSubscribedFrom(other)) {
      return;
    }
    // remove the others subscription to me
    mep.getSubscriptionsFrom().remove(other);
    doCancelSubscription(mep, meid, other, notify);
    if(m_Store.isFriendshipMode()) {
      mep.getSubscriptionsTo().remove(other);
      this.doUnsubscribe(meid,other,false);
    }
  }//cancelSubscriptionOf

  public void probePresence(Presence me, AgentIdentifier aid) {
    m_EventHandler.probe(me, aid);
  }//probePresence(Presence,AgentIdentifier)

  public void probePresence(Presence me, AgentIdentifierList ail) {
    m_EventHandler.probe(me, ail);
  }//probePresence(Presence,AgentIdentifierList)

  private void doUnsubscribe(final AgentIdentifier meid, final AgentIdentifier other, final boolean notify) {

    if (isRegistered(other)) {
      final PresenceImpl otherp = (PresenceImpl) m_Registered.get(other);
      // remove the others subscription from me
      otherp.getSubscriptionsFrom().remove(meid);
      if (otherp.isPresent()) {
        m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(
            m_ServiceAgentProxy.getAuthenticPeer(),
            new Runnable() {
              public void run() {
                if (notify) {

                  PresenceServiceListener psl =
                      (PresenceServiceListener) m_PresenceServiceListeners.get(other);
                  psl.unsubscribed(meid);
                }
                PresenceListener pl = m_EventHandler.getPresenceListener(meid);
                pl.becameAbsent(otherp.getProxy());
              }//run
            }//Runnable
        );//execute
      }
    } else {  //2. not registered
      Map<String, String> params = new HashMap<String, String>();
      params.put("aid1", m_KeyCache.getKeyFor(other));
      params.put("aid2", m_KeyCache.getKeyFor(meid));
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("removeSubscriptionFrom", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "unsubscribeFrom()", ex);
      } finally {
        m_Store.releaseConnection(lc);
      }
    }
  }//unsubscribe

  private void doCancelSubscription(final Presence me, final AgentIdentifier meid, final AgentIdentifier other, final boolean notify) {
    //check if to is registered, requires online handling and notification
    if (isRegistered(other)) {
      PresenceImpl otherp = (PresenceImpl) m_Registered.get(other);
      // remove the others subscription from me
      otherp.getSubscriptionsTo().remove(meid);
      if (otherp.isPresent()) {
        m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED).execute(
            m_ServiceAgentProxy.getAuthenticPeer(),
            new Runnable() {
              public void run() {
                if (notify) {
                  PresenceServiceListener psl =
                      (PresenceServiceListener) m_PresenceServiceListeners.get(other);
                  psl.subscriptionCanceled(meid);
                }
                PresenceListener pl = m_EventHandler.getPresenceListener(meid);
                pl.becameAbsent(me.getProxy());
              }//run
            }//Runnable
        );//execute
      }
    } else {  //2. not registered
      Map<String, String> params = new HashMap<String, String>();
      params.put("aid1", m_KeyCache.getKeyFor(other));
      params.put("aid2", m_KeyCache.getKeyFor(meid));
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("removeSubscriptionTo", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "cancelSubscriptionOf()", ex);
      } finally {
        m_Store.releaseConnection(lc);
      }
    }
  }//doCancelSubscription

  public boolean checkSubscription(Agent caller, Agent ag, AgentIdentifier aid)
      throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CHECK_SUBSCRIPTION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "checkSubscription()", ex);
    }
    final Object o = m_Registered.get(ag.getAgentIdentifier());
    if (o == null) {
      throw new IllegalStateException();
    }
    return ((PresenceImpl) o).isSubscribedTo(aid);
  }//checkSubscription

  /*
    END:SUBSCRIPTIONS
  */

  /*
    BLOCKS
  */

  public void blockInbound(Presence me, AgentIdentifier aid) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    if (mep.allowsInbound(aid)) {
      //need to block it
      mep.getInboundBlocked().add(aid);
      //if subscribed to
      if (mep.isSubscribedTo(aid)) {
        mep.getSubscriptionsTo().remove(aid);
        doUnsubscribe(meid, aid, true);
      }
    }
  }//blockInbound


  public void allowInbound(Presence me, AgentIdentifier aid) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    if (!mep.allowsInbound(aid)) {
      //need to block it
      mep.getInboundBlocked().remove(aid);
    }
  }//allowInbound

  public void blockOutbound(Presence me, AgentIdentifier aid) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    if (mep.allowsOutbound(aid)) {
      //need to block it
      mep.getOutboundBlocked().add(aid);

      //if subscribed from
      if (mep.isSubscribedTo(aid)) {
        mep.getSubscriptionsFrom().remove(aid);
        doCancelSubscription(mep, meid, aid, true);
      }
    }
  }//blockOutbound

  public void allowOutbound(Presence me, AgentIdentifier aid) {
    AgentIdentifier meid = me.getAgentIdentifier();
    PresenceImpl mep = (PresenceImpl) me;
    //ensure me is registered
    if (!isRegistered(meid)) {
      throw new IllegalStateException("Presence " + me.toString() + " is not registered with this service instance.");
    }
    if (!mep.allowsOutbound(aid)) {
      //need to block it
      mep.getOutboundBlocked().remove(aid);
    }
  }//allowOutbound

  private boolean isInboundBlocked(AgentIdentifier a1, AgentIdentifier a2) {
    Map<String, String> params = new HashMap<String, String>();
    params.put("aid1", m_KeyCache.getKeyFor(a1));
    params.put("aid2", m_KeyCache.getKeyFor(a2));
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      ResultSet r = lc.executeQuery("isInboundBlocked", params);
      return (r.next() && r.getInt(1) == 1);
    } catch (Exception ex) {
      Activator.log().error("isInboundBlocked()", ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
    return true;
  }//isInboundBlocked

  /*
   END:BLOCKS
  */

  public Iterator<AgentIdentifier> listAdvertisedPresent(Agent caller, PresenceFilter f) {
    ArrayList<AgentIdentifier> al = new ArrayList<AgentIdentifier>(50);
    for (Iterator iter = m_Registered.values().iterator(); iter.hasNext();) {
      Presence p = (Presence) iter.next();
      if (p.getStatus().isType(PresenceStatusTypes.AdvertisedAvailable)
          || p.getStatus().isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)) {
        if (f == null || f.allows(p.getProxy())) {
          al.add(p.getAgentIdentifier());
        }
      }
    }
    return Collections.unmodifiableList(al).iterator();
  }//listAdvertisedPresent

  public PresenceProxy getAdvertisedPresent(Agent caller, AgentIdentifier aid) {
    Presence p = (Presence) m_Registered.get(aid);
    if (p.getStatus().isType(PresenceStatusTypes.AdvertisedAvailable) ||
        p.getStatus().isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)) {
      return p.getProxy();
    }
    return null;
  }//getAdvertisedPresent

  public Iterator<AgentIdentifier> listRegistered(Agent caller, PresenceFilter f) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), LIST_REGISTERED);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "listRegistered()", ex);
    }
    ArrayList<AgentIdentifier> al = new ArrayList<AgentIdentifier>(50);
    for (Iterator iter = m_Registered.values().iterator(); iter.hasNext();) {
      Presence p = (Presence) iter.next();
      if (f == null || f.allows(p.getProxy())) {
        al.add(p.getAgentIdentifier());
      }
    }
    return Collections.unmodifiableList(al).iterator();
  }//listRegistered

  public PresenceProxy getRegistered(Agent caller, AgentIdentifier aid) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_REGISTERED);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getRegistered()", ex);
    }
    final Object o = m_Registered.get(aid);
    return (o != null) ? ((PresenceImpl) o).getProxy() : null;
  }//getRegistered

  public boolean isAvailable(Agent caller, AgentIdentifier aid) throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), IS_AVAILABLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "isAvailable()", ex);
    }
    final Object o = m_Registered.get(aid);
    return (o != null) && ((Presence) o).isAvailable();
  }//isAvailable

  public boolean isAdvertisedAvailable(Agent caller, AgentIdentifier aid)
      throws SecurityException {

    final Object o = m_Registered.get(aid);
    return (o != null) && ((Presence) o).getStatus().getType().equals(PresenceStatusTypes.AdvertisedAvailable);
  }//isAdvertisedAvailable

  public boolean isRegistered(Agent caller, Presence p) throws SecurityException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), IS_REGISTERED);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "isRegistered()", ex);
    }
    if (m_Registered == null) {
      return false;
    }
    return m_Registered.containsValue(p);
  }//isRegistered

  public int getNumberOfRegistered(Agent caller) {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_NUMBEROF_REGISTERED);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getNumberOfRegistered()", ex);
    }
    return m_Registered.size();
  }//getNumberOfRegistered

  private void ensureRegistered(Presence p) {
    //assert (m_Registered != null):"Collection holding registered presences is null.";
    if (!m_Registered.containsKey(p.getAgentIdentifier())) {
      throw new IllegalStateException("Presence " + p.toString() + " is not registered with this service instance.");
    }
  }//ensureRegistered

  public boolean isRegistered(AgentIdentifier rid) {
    if (m_Registered == null) {
      return false;
    }
    return m_Registered.containsKey(rid);
  }//isRegistered

  public EventHandler getEventHandler() {
    return m_EventHandler;
  }//getEventHandler

  public void doMaintenance(Agent caller)
      throws SecurityException, MaintenanceException {

    //check rights
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }
    try {
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start"));

      //maintain store
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.store"));
      m_Store.maintain(m_ServiceAgentProxy);

      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
    } catch (Exception ex) {
      throw new MaintenanceException(ex.getMessage(), ex);
    }
  }//maintenance

  class EventHandler extends PresenceEventManager {

    public PresenceListener getPresenceListener(AgentIdentifier aid) {
      Object o = m_PresenceServiceListeners.get(aid);
      if (o != null) {
        return (PresenceListener) o;
      } else {
        return null;
      }
    }//getPresenceListener

    public Presence getRegistered(AgentIdentifier aid) {
      Object o = m_Registered.get(aid);
      if (o != null) {
        return (Presence) o;
      } else {
        return null;
      }
    }//getRegistered

    public Iterator getPublicPresenceListeners() {
      synchronized (m_PublicPresenceListeners) {
        ArrayList<PresenceListener> list = new ArrayList<PresenceListener>(m_PublicPresenceListeners.size());
        for (PresenceListener pl : m_PublicPresenceListeners) {
          list.add(pl);
        }
        return list.listIterator();
      }
    }//getPublicPresenceListeners

    public Iterator getPresenceRegistrationListeners() {
      synchronized (m_PresenceRegistrationListeners) {
        ArrayList<PresenceRegistrationListener> list = new ArrayList<PresenceRegistrationListener>(m_PresenceRegistrationListeners.size());
        for (PresenceRegistrationListener pl : m_PresenceRegistrationListeners) {
          list.add(pl);
        }
        return list.listIterator();
      }
    }//getPresenceRegistrationListeners

    public void notifyAllOfUnregister(Presence p) {
      PresenceUnregister pu = null;
      try {
        pu = (PresenceUnregister) m_PresenceUnregisterPool.borrowObject();
        pu.setPresence(p);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), pu);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "notifyAllOfUnregister()", ex);
      }
    }//notifyAllOfUnregister

    public void notifyAllOfStatusUpdate(PresenceImpl p) {
      StatusUpdateNotify notify = null;
      try {
        notify = (StatusUpdateNotify) m_StatusUpdateNotifyPool.borrowObject();
        notify.setPresence(p);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), notify);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "notifyAllOfStatusUpdate()", ex);
      }
    }//notifyAllOfStatusUpdate

    public void notifyAllOfPresenceState(PresenceImpl p) {
      PresenceNotify notify = null;
      try {
        notify = (PresenceNotify) m_PresenceNotifyPool.borrowObject();
        //System.err.println("Borrowed: " + notify.toString());
        //System.err.println("notifyAllOfPresenceState()::" + p.toString());
        notify.setPresence(p);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), notify);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "notifyAllOfPresenceState()", ex);
      }
    }//notifyAllOfPresenceState

    public void notifyAllOfResourceUpdate(PresenceImpl p) {
      ResourceUpdateNotify notify = null;
      try {
        //note that this notifiers return themselves to the pool once run :)
        notify = (ResourceUpdateNotify) m_ResourceUpdateNotifyPool.borrowObject();
        notify.setPresence(p);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), notify);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "notifyAllOfResourceUpdate()", ex);
      }
    }//notifyAllOfResourceUpdate

    public void probe(Presence p, Object toprobe) {
      PresenceProbe probe = null;
      try {
        probe = (PresenceProbe) m_PresenceProbePool.borrowObject();
        probe.setPresence(p);
        probe.setToProbe(toprobe);
        m_Services.getExecutionService(ServiceMediator.NO_WAIT).execute(m_ServiceAgentProxy.getAuthenticPeer(), probe);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "probe()", ex);
      }
    }//probe

  }//PresenceEventManager

  private static Action LIST_REGISTERED = new Action("listRegistered");
  private static Action GET_REGISTERED = new Action("getRegistered");
  private static Action GET_NUMBEROF_REGISTERED = new Action("getNumberOfRegistered");
  private static Action IS_AVAILABLE = new Action("isAvailable");
  private static Action IS_REGISTERED = new Action("isRegistered");
  private static Action ADD_REGISTRATION_LISTENER = new Action("addPresenceRegistrationListener");
  private static Action REMOVE_REGISTRATION_LISTENER = new Action("removePresenceRegistrationListener");
  private static Action CHECK_SUBSCRIPTION = new Action("checkSubscription");

  private static Action[] ACTIONS =
      {LIST_REGISTERED, GET_REGISTERED, GET_NUMBEROF_REGISTERED, IS_AVAILABLE,
          IS_REGISTERED, ADD_REGISTRATION_LISTENER, REMOVE_REGISTRATION_LISTENER,
          CHECK_SUBSCRIPTION, Maintainable.DO_MAINTENANCE};


}//class PresenceServiceImpl
