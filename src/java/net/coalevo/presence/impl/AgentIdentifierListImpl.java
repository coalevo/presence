/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.AgentIdentifierInstanceCache;
import net.coalevo.presence.model.AgentIdentifierList;
import org.apache.commons.collections.list.CursorableLinkedList;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Provides a list of {@link AgentIdentifier} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AgentIdentifierListImpl
    implements AgentIdentifierList {

  private Marker m_LogMarker = MarkerFactory.getMarker(AgentIdentifierListImpl.class.getName());
  private AgentIdentifier m_Owner;
  private Map<String, String> m_Params;
  private CursorableLinkedList m_AgentIdentifiers;
  private PresenceStore m_Store;
  private AgentIdentifierKeyCache m_KeyCache;
  private String m_ListAdd;
  private String m_ListRemove;
  private String m_ListAll;

  public AgentIdentifierListImpl(PresenceStore store, String type, AgentIdentifier owner) {
    m_Store = store;
    m_KeyCache = store.getKeyCache();
    m_Owner = owner;
    prepareList(type);
  }//constructor

  public AgentIdentifier getOwner() {
    return m_Owner;
  }//getOwner

  public boolean add(AgentIdentifier aid) {
    if (!m_AgentIdentifiers.contains(aid) && !m_Owner.equals(aid)) {
      //Update list
      m_AgentIdentifiers.add(aid);
      //update database
      LibraryConnection lc = null;
      synchronized (m_Params) {
        m_Params.put("aid2", m_KeyCache.getKeyFor(aid));
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate(m_ListAdd, m_Params);
          return true;
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "add()", ex);
        } finally {
          m_Store.releaseConnection(lc);
          m_Params.remove("aid2");
        }
      }
    }
    return false;
  }//add

  public boolean remove(AgentIdentifier aid) {
    if (m_AgentIdentifiers.contains(aid)) {
      //update list
      m_AgentIdentifiers.remove(aid);
      //update database
      LibraryConnection lc = null;
      synchronized (m_Params) {
        m_Params.put("aid2", m_KeyCache.getKeyFor(aid));
        try {
          lc = m_Store.leaseConnection();
          lc.executeUpdate(m_ListRemove, m_Params);
          return true;
        } catch (Exception ex) {
          Activator.log().error(m_LogMarker, "remove()", ex);
        } finally {
          m_Store.releaseConnection(lc);
          m_Params.remove("aid2");
        }
      }
    }
    return false;
  }//remove

  public void clear() {
    for (Iterator iterator = m_AgentIdentifiers.listIterator(); iterator.hasNext();) {
      AgentIdentifier agentIdentifier = (AgentIdentifier) iterator.next();
      remove(agentIdentifier);
    }
  }//clear

  public boolean isEmpty() {
    return m_AgentIdentifiers.isEmpty();
  }//isEmtpy

  public boolean contains(AgentIdentifier aid) {
    return m_AgentIdentifiers.contains(aid);
  }//contains

  public Iterator iterator() {
    return m_AgentIdentifiers.listIterator();
  }//iterator

  public String toString() {
    return m_AgentIdentifiers.toString();
  }//toString

  /**
   * Note: This method may cause a deadlock in the database connection pool if
   * used with the automatic key cache. Therefor use of that cache is avoided,
   * and aid's are resolved with an inner join.
   */
  private void refresh() {
    m_AgentIdentifiers.clear();
    LibraryConnection lc = null;
    ResultSet rs = null;
    AgentIdentifierInstanceCache aidcache = m_Store.getAgentIdentifierCache();
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery(m_ListAll, m_Params);
      while (rs.next()) {
        m_AgentIdentifiers.addLast(aidcache.get(rs.getString(1)));
      }
      //System.err.println(m_AgentIdentifiers.toString());
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "prepareList()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//refresh

  private void prepareList(String type) {
    m_AgentIdentifiers = new CursorableLinkedList();
    m_ListAdd = ADD + type;
    m_ListRemove = REMOVE + type;
    m_ListAll = LIST + type;

    m_Params = new HashMap<String, String>();
    m_Params.put("aid", m_KeyCache.getKeyFor(m_Owner));
    m_Params.put("aid1", m_KeyCache.getKeyFor(m_Owner));
    refresh();
  }//prepareList

  private static String ADD = "add";
  private static String REMOVE = "remove";
  private static String LIST = "list";


}//class AgentIdentifierListImpl
