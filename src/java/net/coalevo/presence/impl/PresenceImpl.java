/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.impl;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.presence.model.*;
import net.coalevo.presence.service.PresenceService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class implements a base version of {@link Presence}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceImpl
    implements Presence {

  protected Agent m_Agent;
  protected AgentIdentifier m_AgentIdentifier;
  protected PresenceServiceImpl m_PService;
  protected AtomicBoolean m_Present = new AtomicBoolean(false);
  protected PresenceStatusImpl m_PStatus;
  protected long m_Since = -1;
  protected PresenceServiceImpl.EventHandler m_EventHandler;
  protected CountDownLatch m_Invalidating;
  protected AtomicBoolean m_Ended = new AtomicBoolean(false);

  protected AgentIdentifierList m_InboundBlocks;
  protected AgentIdentifierList m_OutboundBlocks;
  protected AgentIdentifierList m_SubscriptionTo;
  protected AgentIdentifierList m_SubscriptionFrom;
  protected AgentIdentifierList m_RequestedSubscriptions;

  protected List<String> m_Resources;
  private ReentrantLock m_EventLock;

  protected long m_LastStatusUpdate;

  public PresenceImpl(Agent a, PresenceStore ps, PresenceServiceImpl p) {
    m_Agent = a;
    m_AgentIdentifier = a.getAgentIdentifier();
    m_PService = p;
    m_EventHandler = m_PService.getEventHandler();
    m_EventLock = new ReentrantLock(true);
    m_InboundBlocks = new AgentIdentifierListImpl(ps, BLOCK_IN, m_AgentIdentifier);
    m_OutboundBlocks = new AgentIdentifierListImpl(ps, BLOCK_OUT, m_AgentIdentifier);
    m_SubscriptionTo = new AgentIdentifierListImpl(ps, SUBSCRIPTIONTO, m_AgentIdentifier);
    m_SubscriptionFrom = new AgentIdentifierListImpl(ps, SUBSCRIPTIONFROM, m_AgentIdentifier);
    m_RequestedSubscriptions = new AgentIdentifierListImpl(ps, REQUESTED_SUBSCRIPTION, m_AgentIdentifier);
    m_PStatus = new PresenceStatusImpl(PresenceStatusTypes.Available, "");
    m_Resources = Collections.synchronizedList(new ArrayList<String>());
  }//constructor

  public PresenceProxy getProxy() {
    return new PresenceProxyImpl();
  }//getProxy

  public Agent getAgent() {
    return m_Agent;
  }//getAgent

  public void begin() {
    begin(m_PStatus);
  }//begin

  public void begin(PresenceStatus st) {
    if (m_Ended.get() || !m_Present.getAndSet(true)) {
      //1. notify of presence
      if(st instanceof PresenceStatusImpl) {
        m_PStatus = (PresenceStatusImpl) st;
      } else {
        m_PStatus = new PresenceStatusImpl(st.getType(), st.getDescription());
      }
      m_Since = System.currentTimeMillis();
      m_LastStatusUpdate = m_Since;
      m_EventHandler.notifyAllOfPresenceState(this);
      //2. probe all subscribed
      m_PService.probePresence(this, m_SubscriptionTo);

    } else {
      throw new IllegalStateException();
    }
  }//begin

  public void updateStatus(PresenceStatus st) {
    if (m_Ended.get()) {
      throw new IllegalStateException();
    }
    if (m_Present.get()) {
      if (st == null) {
        return;
      }
      if (!isPresent()) {
        throw new IllegalStateException();
      }
      //1. Handle case where the status is not updated directly
      if (st != m_PStatus) {
        m_PStatus.setDescription(st.getDescription());
        m_PStatus.setType(st.getType());
      }
      if (m_PStatus.isUpdated()) {
        m_LastStatusUpdate = System.currentTimeMillis();
        m_PStatus.setUpdated(false);
        m_EventHandler.notifyAllOfStatusUpdate(this);
      }
    }
  }//updateStatus

  public boolean isPresent() {
    return !m_Ended.get() && m_Present.get();
  }//isPresent

  public void end() {
    if (m_Ended.getAndSet(true)) {
      throw new IllegalStateException();
    }
    if (m_Present.getAndSet(false)) {
      m_Resources.clear();
      m_Since = -1;
      m_EventHandler.notifyAllOfPresenceState(this);
    } else {
      throw new IllegalStateException();
    }
  }//end

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

  public PresenceService getService() {
    return m_PService;
  }//getService

  public PresenceStatus getStatus() {
    return m_PStatus;
  }//getStatus

  public long presentSince() {
    return m_Since;
  }//presentSince

  public long getLastStatusUpdate() {
    return m_LastStatusUpdate;
  }//getLastStatusUpdate

  public boolean allowsInbound(AgentIdentifier aid) {
    return !m_InboundBlocks.contains(aid);
  }//allowsInbound

  public boolean isAvailable() {
    return (
        m_PStatus.isType(PresenceStatusTypes.TemporarilyUnavailable)
            || m_PStatus.isType(PresenceStatusTypes.Available)
            || m_PStatus.isType(PresenceStatusTypes.AdvertisedAvailable)
            || m_PStatus.isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)
    );
  }//isAvailable

  public boolean isUnavailable() {
    return m_PStatus.isType(PresenceStatusTypes.Unavailable);
  }//isUnavailable

  public boolean allowsOutbound(AgentIdentifier aid) {
    return !m_OutboundBlocks.contains(aid);
  }//allowsOutbound

  public AgentIdentifierList getSubscriptionsFrom() {
    return m_SubscriptionFrom;
  }//getSubscriptionsFrom

  public boolean isSubscribedFrom(AgentIdentifier aid) {
    return m_SubscriptionFrom.contains(aid);
  }//isSubscribedFrom

  public AgentIdentifierList getSubscriptionsTo() {
    return m_SubscriptionTo;
  }//getSubscriptionsTo

  public boolean isSubscribedTo(AgentIdentifier aid) {
    return m_SubscriptionTo.contains(aid);
  }//isSubscribedTo

  /**
   * ** END SUBSCRIPTIONFROM ****
   */

  public AgentIdentifierList getInboundBlocked() {
    return m_InboundBlocks;
  }//getInboundBlocked

  public AgentIdentifierList getOutboundBlocked() {
    return m_OutboundBlocks;
  }//getOutboundBlocks

  public AgentIdentifierList getRequestedSubscriptions() {
    return m_RequestedSubscriptions;
  }//getRequestedSubscriptions

  public boolean isRequestedSubscription(AgentIdentifier aid) {
    return m_RequestedSubscriptions.contains(aid);
  }//isRequestedSubscription

  /**
   * Presence Resources *
   */
  public void addResource(String name) {
    //check presence
    if (!isPresent()) {
      throw new IllegalStateException();
    }
    //add to resource list
    if (!m_Resources.contains(name)) {
      m_Resources.add(name);
      m_EventHandler.notifyAllOfResourceUpdate(this);
    }
  }//addResource

  public void removeResource(String name) {
    if (!isPresent()) {
      throw new IllegalStateException();
    }
    if (m_Resources.remove(name)) {
      m_EventHandler.notifyAllOfResourceUpdate(this);
    }
  }//removeResource

  public void updateResources(String[] res) {
    if (!isPresent()) {
      throw new IllegalStateException();
    }
    m_Resources.clear();
    m_Resources.addAll(Arrays.asList(res));
    m_EventHandler.notifyAllOfResourceUpdate(this);
  }//updateResources

  public void setResources(String[] res) {
    m_Resources.clear();
    m_Resources.addAll(Arrays.asList(res));
  }//setResources

  public List<String> listResources() {
    return Collections.unmodifiableList((List<String>) m_Resources);
  }//listResources

  public void lockEvents() {
    m_EventLock.lock();
  }//lockEvents

  public void unlockEvents() {
    m_EventLock.unlock();
  }//unlockEvents

  class PresenceProxyImpl implements PresenceProxy {

    private PresenceStatusType m_StatusType;
    private String m_StatusDescription;

    public PresenceProxyImpl() {
      m_StatusType = PresenceImpl.this.getStatus().getType();
      m_StatusDescription = PresenceImpl.this.getStatus().getDescription();
    }//constructor

    public AgentIdentifier getAgentIdentifier() {
      return PresenceImpl.this.getAgentIdentifier();
    }//getAgentIdentifier

    public long presentSince() {
      return PresenceImpl.this.presentSince();
    }//presentSince

    public long getLastStatusUpdate() {
      return PresenceImpl.this.getLastStatusUpdate();
    }//getLastStatusUpdate

    public List<String> listResources() {
      return Collections.unmodifiableList((List<String>) PresenceImpl.this.m_Resources);
    }//listResources

    public PresenceStatusType getStatusType() {
      return m_StatusType;
    }//getStatusType

    public String getStatusDescription() {
      return m_StatusDescription;
    }//getStatusDescription

    public boolean isAvailable() {
      return (
          m_StatusType.equals(PresenceStatusTypes.Available)
              || m_StatusType.equals(PresenceStatusTypes.AdvertisedAvailable)
              || m_StatusType.equals(PresenceStatusTypes.TemporarilyUnavailable)
              || m_PStatus.isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)
      );
    }//isAvailable

    public boolean isUnavailable() {
      return (m_StatusType.equals(PresenceStatusTypes.Unavailable));
    }//isUnavailable

    public boolean isUser() {
      return (m_Agent instanceof UserAgent);
    }//isUser

    public boolean isService() {
      return (m_Agent instanceof ServiceAgent);
    }//isService

  }//inner class PresenceProxy

  public static final String BLOCK_IN = "InboundBlock";
  public static final String BLOCK_OUT = "OutboundBlock";
  public static final String SUBSCRIPTIONFROM = "SubscriptionFrom";
  public static final String SUBSCRIPTIONTO = "SubscriptionTo";
  public static final String REQUESTED_SUBSCRIPTION = "RequestedSubscription";

}//class PresenceImpl
