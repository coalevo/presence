/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.security.model.ServiceAgentProxyListener;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * This class implements a proxy for a {@link Presence} of a service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ServicePresenceProxy
    implements ServiceAgentProxyListener {

  private Logger m_Log;
  private Marker m_LogMarker;

  private PresenceService m_PresenceService;
  private Presence m_Presence;
  private PresenceServiceListener m_PresenceServiceListener;
  private ServiceAgentProxy m_Agent;

  private BundleContext m_BundleContext;
  private CountDownLatch m_PresenceServiceLatch;
  private CountDownLatch m_PresenceLatch;

  private Set<ServicePresenceProxyListener> m_Listeners;

  public ServicePresenceProxy(PresenceServiceListener pl, ServiceAgentProxy sa, Logger log) {
    m_Agent = sa;
    m_Log = log;
    m_LogMarker = MarkerFactory.getMarker("ServicePresenceProxy");
    m_PresenceServiceListener = pl;
    m_Listeners = new HashSet<ServicePresenceProxyListener>();
  }//constructor

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    m_PresenceServiceLatch = new CountDownLatch(1);
    m_PresenceLatch = new CountDownLatch(1);
    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(objectclass=" + PresenceService.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    //Listen to changes, after first time
    m_Agent.getAuthenticPeer();
    m_Agent.addListener(this);
    return true;
  }//prepare

  public void deactivate() {
    if (m_Agent != null) {
      m_Agent.removeListener(this);
    }
    if (m_PresenceService != null && m_Presence != null) {
      try {
        if (m_Presence.isPresent()) {
          m_Presence.end();
        }
      } catch (Exception ex) {
        m_Log.error(m_LogMarker, "deactivate()", ex);
      }
      try {
        m_PresenceService.unregister(m_Presence);
      } catch (Exception ex) {
        m_Log.error(m_LogMarker, "deactivate()", ex);
      }
      m_PresenceService = null;
      m_Presence = null;
    }
    m_Listeners.clear();
    m_BundleContext = null;
  }//deactivate

  public Presence getPresence() {
    try {
      m_PresenceLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "getPresence()", e);
    }
    return m_Presence;
  }//getPresence

  public PresenceService getPresenceService() {
    try {
      m_PresenceServiceLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "getPresenceService()", e);
    }
    return m_PresenceService;
  }//getPresenceService

  public void updatedServiceAgent() {
    m_Log.debug(m_LogMarker, "updatedServiceAgent()");
    ensurePresence();
  }//updated

  public void addListener(ServicePresenceProxyListener l) {
    synchronized (m_Listeners) {
      m_Listeners.add(l);
    }
  }//addListener

  public void removeListener(ServicePresenceProxyListener l) {
    synchronized (m_Listeners) {
      m_Listeners.remove(l);
    }
  }//removeListener

  private void notifyListeners() {
    synchronized (m_Listeners) {
      for (ServicePresenceProxyListener l : m_Listeners) {
        l.updatedPresence();
      }
    }
  }//notifyListeners

  private void ensurePresence() {
    Thread t = new Thread(new Runnable() {
      public void run() {
        if (m_Presence == null) {
          initPresence();
        } else {
          PresenceService ps = getPresenceService();
          if(ps.isRegistered(m_Agent.getAuthenticPeer(),m_Presence)) {
            // Agent is no longer authentic
            try {
              if(m_Presence.isPresent()) {
                m_Presence.end();
              }
            } catch(Exception ex) {
              m_Log.error(m_LogMarker, "ensurePresence()", ex);
            }
            try {
              ps.unregister(m_Presence);
            } catch (Exception ex) {
              m_Log.error(m_LogMarker, "ensurePresence()", ex);
            }
          }
          updatePresence();
        }
      }//Runnable
    });//Thread
    t.setContextClassLoader(this.getClass().getClassLoader());
    t.start();
  }//ensurePresence

  private void initPresence() {
    m_Log.debug(m_LogMarker, "initPresence()");
    try {
      m_Presence = getPresenceService().register(m_Agent.getAuthenticPeer(), m_PresenceServiceListener);
      m_Presence.begin();
      m_PresenceLatch.countDown();
    } catch (Exception ex) {
      m_Log.error(m_LogMarker, "initPresence()", ex);
      return;
    }
  }//initPresence

  private void updatePresence() {
    m_Log.debug(m_LogMarker, "updatePresence()");
    try {
      if(m_Presence.isPresent()) {
        String desc = m_Presence.getStatus().getDescription();
        String type = ServicePresenceProxy.this.m_Presence.getStatus().getType().getIdentifier();

        m_Presence = getPresenceService().register(m_Agent.getAuthenticPeer(), m_PresenceServiceListener);

        PresenceStatus pst = m_Presence.getStatus();
        pst.setDescription(desc);
        pst.setType(PresenceStatusTypes.getPresenceStatusType(type));
        m_Presence.updateStatus(pst);
        m_PresenceLatch.countDown();
      }
      notifyListeners();
    } catch (Exception ex) {
      m_Log.error("updatePresence()", ex);
    }
  }//updatePresence

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof PresenceService) {
            m_PresenceService = (PresenceService) o;
            m_PresenceServiceLatch.countDown();
            ensurePresence();
            m_Log.debug(m_LogMarker, "serviceChanged()::registered::PresenceService,Presence");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof PresenceService) {
            m_PresenceService = null;
            m_Presence = null;
            m_PresenceServiceLatch = new CountDownLatch(1);
            m_PresenceLatch = new CountDownLatch(1);
            m_Log.debug(m_LogMarker, "serviceChanged()::unregistered::PresenceService,Presence");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

}//class ServicePresenceProxy
