/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

/**
 * This class defines all valid {@link PresenceStatusType} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)

 */
public final class PresenceStatusTypes {

  /**
   * Defines the {@link PresenceStatusType} <tt>Available</tt>.
   */
  public static final PresenceStatusType Available = new TypeImpl("available");

  /**
   * Defines the {@link PresenceStatusType} <tt>AdvertisedAvailable</tt>.
   */
  public static final PresenceStatusType AdvertisedAvailable = new TypeImpl("ad-available");

  /**
   * Defines the {@link PresenceStatusType} <tt>AdvertisedTemporarilyUnavailable</tt>.
   */
  public static final PresenceStatusType AdvertisedTemporarilyUnavailable = new TypeImpl("ad-tmp-unavailable");

  /**
   * Defines the {@link PresenceStatusType} <tt>Unavailable</tt>.
   */
  public static final PresenceStatusType TemporarilyUnavailable = new TypeImpl("tmp-unavailable");

  /**
   * Defines the {@link PresenceStatusType} <tt>Unvailable</tt>.
   */
  public static final PresenceStatusType Unavailable = new TypeImpl("unavailable");


  /**
   * Returns a {@link PresenceStatusType} for a given string.
   * @param str the string defining the type.
   * @return a {@link PresenceStatusType}.
   */
  public static PresenceStatusType getPresenceStatusType(String str) {
    if("available".equals(str)) {
      return Available;
    }
    if("ad-available".equals(str)) {
      return AdvertisedAvailable;
    }
    if("ad-tmp-unavailable".equals(str)) {
      return AdvertisedTemporarilyUnavailable;
    }
    if("tmp-unavailable".equals(str)) {
      return TemporarilyUnavailable;
    }
    if("unavailable".equals(str)) {
      return Unavailable;
    }
    return null;
  }//getPresenceStatusType

  static final class TypeImpl implements PresenceStatusType {

    private final String m_Identifier;

    protected TypeImpl(String id) {
      m_Identifier = id;
    }//constructor

    public String getIdentifier() {
      return m_Identifier;
    }//getIdentifier

    public String toString() {
      return m_Identifier;
    }//toString

    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof TypeImpl)) return false;

      final TypeImpl type = (TypeImpl) o;

      if (!m_Identifier.equals(type.m_Identifier)) return false;

      return true;
    }//equals

    public int hashCode() {
      return (m_Identifier != null ? m_Identifier.hashCode() : 0);
    }//hashCode

  }//inner class TypeImpl

}//class PresenceStatusTypes
