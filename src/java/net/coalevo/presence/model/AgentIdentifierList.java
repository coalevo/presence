/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Iterator;

/**
 * Defines a simple list of {@link AgentIdentifier} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface AgentIdentifierList {

  /**
   * Adds an {@link AgentIdentifier} instance to this list.
   *
   * @param aid an {@link AgentIdentifier} instance.
   * @return true if added, false otherwise.
   */
  public boolean add(AgentIdentifier aid);

  /**
   * Removes an {@link AgentIdentifier} instance from this
   * list.
   * @param aid an {@link AgentIdentifier} instance.
   * @return true if removed, false otherwise.
   */
  public boolean remove(AgentIdentifier aid);

  /**
   * Removes all list entries.
   */
  public void clear();

  /**
   * Tests if this list contains a given {@link AgentIdentifier}
   * @param aid an {@link AgentIdentifier} instance.
   * @return true if contained, false otherwise.
   */
  public boolean contains(AgentIdentifier aid);

  /**
   * Returns an {@link Iterator} over this list.
   * @return an <tt>Iterator</tt>.
   */
  public Iterator iterator();

  /**
   * Tests if this list is empty.
   * @return true if empty, false otherwise.
   */
  public boolean isEmpty();
  
}//interface AgentIdentifierList
