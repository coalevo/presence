/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.foundation.model.AgentIdentifier;

/**
 * Defines the interface for a router of presence events.
 * <p/>
 * An implementation should provide a bounded queue for
 * incoming presence notifications from the network, which
 * should be handled automatically if when the router
 * is registered.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceEventRouter {

  /**
   * Returns the domain this router is working for.
   * @return the domain identifier that this router handles.
   */
  public String getDomain();

  /**
   * Routes a public {@link Presence} status update
   * according to router rules. The outbound blocks
   * of the presence should be taken into account
   * somehow.
   *
   * @param p the local {@link Presence} that changed its status.
   */
  public void routeStatusUpdated(PresenceProxy p);

  /**
   * Routes a directed {@link Presence} status update
   * to a given {@link AgentIdentifier}.
   *
   * @param p the local {@link Presence} that changed its status.
   * @param aid  the {@link AgentIdentifier} to be notified.
   */
  public void routeStatusUpdated(PresenceProxy p, AgentIdentifier aid);

  /**
   * Routes a public {@link Presence} resource update
   * according to router rules. The outbound blocks
   * of the presence should be taken into account
   * somehow.
   *
   * @param p the local {@link Presence} that changed its status.
   */
  public void routeResourcesUpdated(PresenceProxy p);

  /**
   * Routes a directed {@link Presence} resource update
   * to a given {@link AgentIdentifier}.
   *
   * @param p the local {@link Presence} that changed its status.
   * @param aid  the {@link AgentIdentifier} to be notified.
   */
  public void routeResourcesUpdated(PresenceProxy p, AgentIdentifier aid);

  /**
   * Routes a public {@link Presence} event according to
   * router rules. The outbound blocks of the presence should
   * be taken into account somehow.
   *
   * @param p the local {@link Presence} that became present.
   */
  public void routeBecamePresent(PresenceProxy p);

  /**
   * Routes a directed {@link Presence} event to a given
   * {@link AgentIdentifier}.
   *
   * @param p the local {@link Presence} that became present.
   * @param aid  the {@link AgentIdentifier} to be notified.
   */
  public void routeBecamePresent(PresenceProxy p,AgentIdentifier aid);

  /**
   * Routes a public {@link Presence} event according to
   * router rules. The outbound blocks of the presence should
   * be taken into account somehow.
   *
   * @param p the local {@link Presence} that became absent.
   */
  public void routeBecameAbsent(PresenceProxy p);

  /**
   * Routes a directed {@link Presence} event to a given
   * {@link AgentIdentifier}.
   *
   * @param p the local {@link Presence} that became absent.
   * @param aid  the {@link AgentIdentifier} to be notified.
   */
  public void routeBecameAbsent(PresenceProxy p,AgentIdentifier aid);

  /**
   * Routes a probe response.
   *
   * @param prober a {@link AgentIdentifier}.
   * @param probed a {@link PresenceProxy}.
   */
  public void routeProbeResponse(AgentIdentifier prober, PresenceProxy probed);

  /**
   * Routes a probe request.
   *
   * @param prober a {@link PresenceProxy}.
   * @param probed an {@link AgentIdentifier}.
   */
  public void routeProbeRequest(PresenceProxy prober, AgentIdentifier probed);

  /**
   * Returns the next {@link PresenceEvent} received.
   *
   * @return a {@link PresenceEvent} instance.
   */
   public PresenceEvent dequeueNextEvent();

  /**
   * Tests if this <tt>PresenceEventRouter</tt> is active.
   *
   * @return true if active, false otherwise.
   */
  public boolean isActive();

}//interface PresenceEventRouter
