/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.service.PresenceService;


/**
 * This interface defines a listener for
 * asynchronuous events from the {@link PresenceService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceServiceListener
    extends PresenceListener {

  /**
   * Sets the owner of this listener.
   * @param p the owner of this listener.
   */
  public void setOwner(Presence p);
  
  /**
   * A probed {@link AgentIdentifier} was found.
   *
   * @param p the proxy for the {@link Presence} associated
   *          with the {@link AgentIdentifier} that was encountered.
   */
  public void found(PresenceProxy p);

  /**
   * The subscription to the {@link AgentIdentifier} identified by the
   * {@link AgentIdentifier} was allowed.
   *
   * @param rid a {@link AgentIdentifier}.
   */
  public void subscriptionAllowed(AgentIdentifier rid);

  /**
   * The subscription to the {@link AgentIdentifier} identified by the
   * {@link AgentIdentifier} was denied.
   *
   * @param rid    a {@link AgentIdentifier}.
   * @param reason an optional <tt>String</tt> specifying a reason.
   */
  public void subscriptionDenied(AgentIdentifier rid, String reason);

  /**
   * The subscription to the {@link AgentIdentifier} identified by the
   * {@link AgentIdentifier} was canceled.
   *
   * @param rid a {@link AgentIdentifier}.
   */
  public void subscriptionCanceled(AgentIdentifier rid);

  /**
   * The {@link AgentIdentifier} identified by the
   * {@link AgentIdentifier} unsubscribed.
   *
   * @param rid a {@link AgentIdentifier}.
   */
  public void unsubscribed(AgentIdentifier rid);

}//interface PresenceServiceListener
