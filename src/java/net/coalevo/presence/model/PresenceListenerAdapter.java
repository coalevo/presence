/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides an abstract base class for deriving {@link PresenceListener}
 * implementations.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class PresenceListenerAdapter
    implements PresenceListener {

  protected Presence m_Owner;
  protected AtomicBoolean m_Active = new AtomicBoolean(true);

  public Presence getOwner() {
    return m_Owner;
  }//getOwner

  public void becamePresent(PresenceProxy p) {
  }//becamePresent

  public void becameAbsent(PresenceProxy p) {
  }//becameAbsent

  public void statusUpdated(PresenceProxy p) {
  }//statusUpdated

  public void resourcesUpdated(PresenceProxy p) {
  }//resourcesUpdated

  public void requestedSubscription(PresenceProxy p) {
  }//requestedSubscription

  public void receivedPresence(PresenceProxy p) {
  }//receivedPresence

  public boolean isActive() {
    return m_Active.get();
  }//isActive

  /**
   * Sets the flag that determines if this listener is
   * active.
   *
   * @param b true if active, false otherwise.
   */
  public void setActive(boolean b) {
    m_Active.set(b);
  }//setActive

}//class PresenceListenerAdapter
