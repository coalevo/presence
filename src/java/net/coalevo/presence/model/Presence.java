/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.service.PresenceService;

import java.util.List;


/**
 * This interface defines a <tt>Presence</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Presence {

  /**
   * Returns the {@link Agent} associated with this
   * <tt>Presence</tt>.
   * @return an {@link Agent} instance.
   */
  public Agent getAgent();

  /**
   * Initiates this <tt>Presence</tt> with
   * {@link PresenceStatusTypes#Available}.
   */
  public void begin();

  /**
   * Initiates this <tt>Presence</tt> with a given status.
   *
   * @param st a {@link PresenceStatus}.
   */
  public void begin(PresenceStatus st);

  /**
   * Updates this <tt>Presence</tt> to a given status.
   *
   * @param st a {@link PresenceStatus}.
   */
  public void updateStatus(PresenceStatus st);

  /**
   * Tests if the agent associated with
   * this <tt>Presence</tt> is present.
   *
   * @return true if present, false otherwise.
   */
  public boolean isPresent();

  /**
   * Ends this <tt>Presence</tt>.
   */
  public void end();

  /**
   * Returns the {@link AgentIdentifier} associated with
   * this <tt>Presence</tt>.
   *
   * @return a {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getAgentIdentifier();

  /**
   * Returns the {@link PresenceStatus} of the
   * proxied {@link Presence}.
   *
   * @return a {@link PresenceStatus} instance.
   */
  public PresenceStatus getStatus();

  /**
   * Returns since when the proxied {@link Presence}
   * is present.
   *
   * @return a UTC timestamp of the moment when the {@link Presence}
   *         became present.
   */
  public long presentSince();

  /**
   * Returns since when the proxied {@link Presence}
   * is in the current status.
   *
   * @return a UTC timestamp of the moment when the {@link Presence}
   *         assumed the current status.
   */
  public long getLastStatusUpdate();

  /**
   * Returns a list of named resources of the proxied
   * {@link Presence}.
   *
   * @return a non-modifiable list of named resources (String).
   */
  public List listResources();

  /**
   * Adds a resource.
   * <p/>
   * Note that this change will be automatically propagated
   * by an event.
   *
   * @param name the resources name.
   */
  public void addResource(String name);

  /**
   * Removes a resource.
   * <p/>
   * Note that this change will be automatically propagated
   * by an event.
   *
   * @param name the resources name.
   */
  public void removeResource(String name);

  /**
   * Updates the resources.
   * <p/>
   * Note that this change will be automatically propagated
   * by an event.
   *
   * @param res the new resources as <tt>String[]</tt>.
   */
  public void updateResources(String[] res);

  /**
   * Sets the resources.
   * <p/>
   * Note that this change will NOT be propagated
   * by an event. The idea is that this method can be used
   * before becoming present.
   *
   * @param res the new resources as <tt>String[]</tt>.
   */
  public void setResources(String[] res);

  /**
   * Returns the {@link PresenceService} this
   * <tt>Presence</tt> is registered with.
   *
   * @return an {@link PresenceService} instance.
   */
  public PresenceService getService();

  /**
   * Returns the {@link PresenceProxy} for this
   * <tt>Presence</tt>.
   *
   * @return a {@link PresenceProxy} instance.
   */
  public PresenceProxy getProxy();

  /**
   * Returns an {@link AgentIdentifierList}, holding
   * {@link AgentIdentifier}s of the {@link Agent}s the
   * {@link Presence} is subscribed to.
   *
   * @return an {@link AgentIdentifierList} instance.
   */
  public AgentIdentifierList getSubscriptionsFrom();

  /**
   * Returns an {@link AgentIdentifierList}, holding
   * {@link AgentIdentifier}s of the {@link Agent}s that
   * are subscribed to this presence.
   *
   * @return an {@link AgentIdentifierList} instance.
   */
  public AgentIdentifierList getSubscriptionsTo();

  /**
   * Returns an {@link AgentIdentifierList}, holding
   * {@link AgentIdentifier}s of the {@link Agent}s that
   * requested subscription to this {@link Presence}.
   *
   * @return an {@link AgentIdentifierList} instance.
   */
  public AgentIdentifierList getRequestedSubscriptions();

  /**
   * Returns an {@link AgentIdentifierList}, holding
   * {@link AgentIdentifier}s of the {@link Agent}s whoose
   * notifications are blocked.
   *
   * @return an {@link AgentIdentifierList} instance.
   */
  public AgentIdentifierList getInboundBlocked();

  /**
   * Returns an {@link AgentIdentifierList}, holding
   * {@link AgentIdentifier}s of the {@link Agent}s that
   * will not be notified under any circumstance.
   *
   * @return an {@link AgentIdentifierList} instance.
   */
  public AgentIdentifierList getOutboundBlocked();

  /**
   * Tests if this <tt>Presence</tt> allows outbound
   * notifications from the given {@link AgentIdentifier}.
   *
   * @param aid an {@link AgentIdentifierList} instance.
   * @return true if allowed, false otherwise.
   */
  public boolean allowsOutbound(AgentIdentifier aid);

  /**
   * Tests if this <tt>Presence</tt> allows inbound
   * notifications from the given {@link AgentIdentifier}.
   * This is a convenience method for
   * <tt>getInboundBlocked().contains(AgentIdentifier)</tt>.
   *
   * @param aid an {@link AgentIdentifierList} instance.
   * @return true if allowed, false otherwise.
   */
  public boolean allowsInbound(AgentIdentifier aid);

  /**
   * Tests if this <tt>PresenceStatus</tt> represents
   * availability.
   *
   * @return true if available, false otherwise.
   */
  public boolean isAvailable();

  /**
   * Tests if this <tt>PresenceStatus</tt> represents
   * unavailability.
   *
   * @return true if unavailable, false otherwise.
   */
  public boolean isUnavailable();

}//interface Presence
