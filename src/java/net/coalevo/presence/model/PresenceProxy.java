/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.service.PresenceService;

import java.util.List;

/**
 * This interface defines a proxy for a
 * {@link Presence}.
 * <p/>
 * The {@link Presence} instance belonging to a
 * user or service due to the registration with the
 * {@link PresenceService} should never be shared
 * with untrusted parties. Whenever sharing is required
 * with untrusted parties (notifications, etc.), the
 * {@link PresenceProxy} should be given as reference,
 * which can be obtained using {@link Presence#getProxy()},
 * and provides only public accessible information.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceProxy {

  /**
   * Returns the {@link AgentIdentifier} that identifies
   * the resource of the proxied {@link Presence}.
   *
   * @return a {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getAgentIdentifier();

  /**
   * Returns since when the proxied {@link Presence}
   * is present.
   *
   * @return a UTC timestamp of the moment when the {@link Presence}
   *         became present.
   */
  public long presentSince();

  /**
   * Returns since when the proxied {@link Presence}
   * is in the current status.
   *
   * @return a UTC timestamp of the moment when the {@link Presence}
   *         assumed the current status.
   */
  public long getLastStatusUpdate();  

  /**
   * Returns a list of named resources of the proxied
   * {@link Presence}.
   *
   * @return a non-modifiable list of named resources (String).
   */
  public List listResources();

  /**
   * Returns the {@link PresenceStatusType} of the proxied {@link Presence}.
   *
   * @return a {@link PresenceStatusType}.
   */
  public PresenceStatusType getStatusType();

  /**
   * Returns the status description of the proxied {@link Presence}.
   *
   * @return the description as <tt>String</tt>.
   */
  public String getStatusDescription();

  /**
   * Tests if this <tt>PresenceStatus</tt> represents
   * availability.
   * @return true if available, false otherwise.
   */
  public boolean isAvailable();

   /**
   * Tests if this <tt>PresenceStatus</tt> represents
   * unavailability.
   * @return true if unavailable, false otherwise.
   */
  public boolean isUnavailable();

  /**
   * Tests if this presence corresponds to a user.
   * @return true if corresponds to a user, false otherwise.
   */
  public boolean isUser();

  /**
   * Tests if this presence corresponds to a service.
   * @return true if corresponds to a service, false otherwise.
   */
  public boolean isService();

}//interface PresenceProxy
