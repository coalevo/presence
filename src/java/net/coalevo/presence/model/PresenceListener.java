/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.presence.service.PresenceService;


/**
 * Defines the interface for a <tt>PresenceListener</tt>
 * that can handle asynchronuous presence related events
 * from the {@link PresenceService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceListener {

  /**
   * Returns the {@link Presence} associated
   * with this <tt>PresenceListener</tt>.
   *
   * @return a {@link Presence}.
   */
  public Presence getOwner();

  /**
   * A subscribed {@link Presence} became present.
   *
   * @param p the proxy for the {@link Presence} that became present.
   */
  public void becamePresent(PresenceProxy p);

  /**
   * A subscribed {@link Presence} became absent.
   *
   * @param p the proxy for the {@link Presence} that became absent.
   */
  public void becameAbsent(PresenceProxy p);

  /**
   * A subscribed {@link Presence} updated its status.
   *
   * @param p the proxy for the {@link Presence} that changed its status.
   */
  public void statusUpdated(PresenceProxy p);

  /**
   * A subscribed {@link Presence} updated its resources.
   * @param p the proxy for the {@link Presence}  that changed its resources.
   */
  public void resourcesUpdated(PresenceProxy p);

  /***
   * Another {@link Presence} requested subscription.
   *
   * @param p the proxy for the {@link Presence} that requested subscription.
   */
  public void requestedSubscription(PresenceProxy p);

  /**
   * A {@link Presence} was received, likely due to a probe.
   *
   * @param p the proxy for the {@link Presence} that was probed.
   */
  public void receivedPresence(PresenceProxy p);

  /**
   * Tests if this <tt>PresenceListener</tt> is active.
   * @return true if active, false otherwise.
   */
  public boolean isActive();

}//interface PresenceListener
