/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Provides a base implementation for {@link PresenceListener}
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BasePresenceListener
    implements PresenceListener {

  private Presence m_Owner;
  private AtomicBoolean m_Active;

  public BasePresenceListener(Presence p) {
    m_Owner = p;
    m_Active = new AtomicBoolean(false);
  }//BasePresenceListener

  public Presence getOwner() {
    return m_Owner;
  }//getOwner

  public boolean isActive() {
    return m_Active.get();
  }//isActive

  public void setActive(boolean b) {
    m_Active.set(b);
  }//setActive
  
}//class BasePresenceListener
