/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

import net.coalevo.foundation.model.AgentIdentifier;

/**
 * Defines an interface for a presence event.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceEvent {

  /**
   * Tests if this <tt>PresenceEvent</tt>
   * is public.
   * <p/>
   * Non public instances should carry
   * a target {@link #getTarget()}.
   *
   * @return true if public, false otherwise.
   */
  public boolean isPublic();

  /**
   * Tests if this <tt>PresenceEvent</tt> is a probe.
   *
   * @return true if probe, false otherwise.
   */
  public boolean isProbe();

  /**
   * Tests if this <tt>PresenceEvent</tt>
   * indicates that a {@link Presence} became
   * present.
   *
   * @return true if a {@link Presence} became present,
   *         false otherwise.
   */
  public boolean isBecamePresent();

  /**
   * Tests if this <tt>PresenceEvent</tt>
   * indicates that a {@link Presence} became
   * absent.
   *
   * @return true if a {@link Presence} became absent,
   *         false otherwise.
   */
  public boolean isBecameAbsent();

  /**
   * Tests if this <tt>PresenceEvent</tt>
   * indicates that a {@link Presence} updated
   * its status.
   *
   * @return true if a {@link Presence} updated it's status,
   *         false otherwise.
   */
  public boolean isStatusUpdate();

  /**
   * Returns the target {@link AgentIdentifier}.
   *
   * @return an {@link AgentIdentifier} instance.
   */
  public AgentIdentifier getTarget();

  /**
   * Returns the {@link PresenceProxy} related with
   * this <tt>PresenceEvent</tt>.
   * Any instance should carry
   * a target {@link #getTarget()}.
   *
   * @return a {@link PresenceProxy} instance.
   */
  public PresenceProxy getPresence();

}//interface PresenceEvent
