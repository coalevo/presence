/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.model;

/**
 * This interface defines a <tt>PresenceStatus</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceStatus {

  /**
   * Returns the description of this <tt>PresenceStatus</tt>.
   *
   * @return the description as <tt>String</tt>.
   */
  public String getDescription();

  /**
   * Sets the description of this <tt>PresenceStatus</tt>.
   *
   * @param str the description as <tt>String</tt>.
   */
  public void setDescription(String str);

  /**
   * Returns the {@link PresenceStatusType} of this <tt>PresenceStatus</tt>.
   *
   * @return a {@link PresenceStatusType}.
   */
  public PresenceStatusType getType();

  /**
   * Sets the {@link PresenceStatusType} of this <tt>PresenceStatus</tt>.
   * @param type a {@link PresenceStatusType}.
   */
  public void setType(PresenceStatusType type);

  /**
   * Tests if this <tt>PresenceStatus</tt> is of a given type.
   *
   * @param type a {@link PresenceStatusType}.
   * @return true if of the given type, false otherwise.
   */
  public boolean isType(PresenceStatusType type);

  /**
   * Tests if this <tt>PresenceStatus</tt> represents
   * availability.
   * @return true if available, false otherwise.
   */
  public boolean isAvailable();

   /**
   * Tests if this <tt>PresenceStatus</tt> represents
   * unavailability.
   * @return true if unavailable, false otherwise.
   */
  public boolean isUnavailable();

}//interface PresenceStatus
