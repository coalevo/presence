/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.presence.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Service;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.presence.model.*;

import java.util.Iterator;
import java.util.List;

/**
 * This interface defines the contract for a <tt>PresenceService</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceService
    extends Service {

  /**
   * Registers with this <tt>PresenceService</tt>.
   * <p>
   * To become present, use {@link Presence#begin()}
   * or {@link Presence#begin(PresenceStatus)}. As a
   * consequence the registered {@link PresenceServiceListener}
   * will receive probed online subscriber presences.
   * </p>
   * <p>
   * Note that upon successfull registration of a {@link UserAgent}
   * with a session, a listener will be attached that automatically
   * handles session invalidation (i.e. state transition to absent
   * and removal of the registration and listeners).
   * </p>
   *
   * @param a   an {@link Agent}.
   * @param psl an {@link PresenceServiceListener}.
   * @return a {@link Presence} instance for the {@link Agent}
   *         within this <tt>PresenceService</tt>.
   */
  public Presence register(Agent a, PresenceServiceListener psl);

  /**
   * Unregisters the given {@link Presence}.
   *
   * @param p the {@link Presence} of a formerly registered {@link Agent}.
   * @throws IllegalStateException if the {@link Presence} instance was not obtained from
   *                               this <tt>PresenceService</tt> or is still present.
   */
  public void unregister(Presence p);

  /**
   * Registers a {@link PresenceListener} instance for public
   * presences.
   *
   * @param pl a {@link PresenceListener} instance.
   */
  public void registerPublicPresenceListener(PresenceListener pl);

  /**
   * Unegisters a {@link PresenceListener} instance for public
   * presences.
   * @param pl a {@link PresenceListener} instance.
   */
  public void unregisterPublicPresenceListener(PresenceListener pl);

  /**
   * Requests subscription to another {@link Agent}'s {@link Presence}.
   *
   * @param me the {@link Presence} of the requesting {@link Agent}.
   * @param to the {@link net.coalevo.foundation.model.AgentIdentifier} of the
   *        {@link Agent} to be requested subscription to its {@link Presence}.
   * @return true if request was successful, false otherwise.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public boolean requestSubscriptionTo(Presence me, AgentIdentifier to);

  /**
   * Check subscription of an agent to another agent.
   * <p>
   * The passed in agent should be currently registered.
   * </p>
   *
   * @param caller the calling {@link Agent}.
   * @param ag     an {@link Agent} that should be currently registered.
   * @param aid    the identifier for the agent to check subscription for.
   * @return true if subscribed, false otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                              the corresponding action.
   * @throws IllegalStateException if the {@link Agent} instance is not currently
   *                               registered with this <tt>PresenceService</tt>.
   */
  public boolean checkSubscription(Agent caller, Agent ag, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Lists subscriptions requested to other {@link Agent}s {@link Presence}s.
   *
   * @param me the {@link Presence} of the requesting {@link Agent}.
   * @return a list of {@link net.coalevo.foundation.model.AgentIdentifier} of the
   *        {@link Agent}s that have been requested subscription to their {@link Presence}.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public List<AgentIdentifier> listPendingRequests(Presence me);

  /**
   * Unsubscribes from the {@link Presence} of another {@link Agent}.
   *
   * @param me     the {@link Presence} of the unsubscribing {@link Agent}.
   * @param from   the {@link AgentIdentifier} of the {@link Agent} to
   *               be unsubscribed.
   * @param notify true if the other {@link Agent} should be notified of
   *               the unsubscribe, false otherwise.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void unsubscribeFrom(Presence me, AgentIdentifier from, boolean notify);

  /**
   * Cancels the subscription of another {@link Agent}.
   *
   * @param me     the {@link Presence} of the cancelling {@link Agent}.
   * @param of   the {@link AgentIdentifier} of the {@link Agent}
   *               whose subscription is to be canceled.
   * @param notify true if the other {@link Agent} should be notified of
   *               the cancelled subscription, false otherwise.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void cancelSubscriptionOf(Presence me, AgentIdentifier of, boolean notify);

  /**
   * Allows the subscription of an {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param aid the {@link AgentIdentifier} of the {@link Agent} whose
   *            subscription is to be allowed.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void allowSubscription(Presence me, AgentIdentifier aid);

  /**
   * Denies the subscription of an {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param aid the {@link AgentIdentifier} of the {@link Agent} whose
   *            subscription is to be denied.
   * @param reason a reason.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void denySubscription(Presence me, AgentIdentifier aid,String reason);

  /**
   * Actively probes for the {@link Presence} of a {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param aid the {@link AgentIdentifier} of the {@link Agent} to
   *            be probed for presence.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void probePresence(Presence me, AgentIdentifier aid);

  /**
   * Actively probes for the {@link Presence} of all {@link Agent}
   * instances identified by the {@link AgentIdentifierList} entries.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param ail the {@link AgentIdentifierList} holding all {@link AgentIdentifier}
   *            to instances to be probed.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void probePresence(Presence me, AgentIdentifierList ail);

  /**
   * Blocks the inbound presence notifications of a {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param aid the {@link AgentIdentifier} of the {@link Agent} whose
   *            inbound notifications are to be blocked.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   *
   */
  public void blockInbound(Presence me, AgentIdentifier aid);

  /**
   * Blocks the outbound presence notifications to a {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param aid the {@link AgentIdentifier} of the {@link Agent} to
   *            which outbound notifications are to be blocked.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void blockOutbound(Presence me, AgentIdentifier aid);

  /**
   * Allows the inbound presence notifications of a {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @param aid the {@link AgentIdentifier} of the {@link Agent} whose
   *            inbound notifications are to be allowed.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void allowInbound(Presence me, AgentIdentifier aid);

  /**
   * Allows the outbound presence notifications to a {@link Agent}
   * identified by a {@link AgentIdentifier}.
   *
   * @param aid the {@link AgentIdentifier} of the {@link Agent} to
   *            which outbound notification are to be allowed.
   * @param me  the {@link Presence} of the requesting {@link Agent}.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public void allowOutbound(Presence me, AgentIdentifier aid);

  /**
   * Returns an <tt>Iterator</tt> over {@link PresenceProxy} instances
   * matched by the given {@link PresenceFilter} instance.
   *
   * @param caller an {@link Agent} instance.
   * @param f  a {@link PresenceFilter}.
   * @return an iterator over {@link AgentIdentifier} instances.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   */
  public Iterator<AgentIdentifier> listAdvertisedPresent(Agent caller, PresenceFilter f);

  /**
   * Returns the {@link PresenceProxy} of a given {@link AgentIdentifier}
   * if the presence status is advertised available.
   *
   * @param caller an {@link Agent} instance.
   * @param aid an {@link AgentIdentifier} instance.
   * @return a {@link PresenceProxy} instance.
   * @throws SecurityException    if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public PresenceProxy getAdvertisedPresent(Agent caller, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Returns an <tt>Iterator</tt> over the registered {@link PresenceProxy}
   * instances matched by the given {@link PresenceFilter} instance.
   *
   * @param caller an {@link Agent} instance.
   * @param f  a {@link PresenceFilter}.
   * @return an iterator over {@link AgentIdentifier} instances.
   * @throws IllegalStateException if the {@link Presence} instance was not
   *                               obtained from this <tt>PresenceService</tt>.
   * @throws SecurityException    if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public Iterator<AgentIdentifier> listRegistered(Agent caller, PresenceFilter f) throws SecurityException;

  /**
   * Returns the number of registered {@link Agent} instances
   * @param caller an {@link Agent} instance.
   * @return a number as <tt>int</tt>.
   * @throws SecurityException    if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public int getNumberOfRegistered(Agent caller) throws SecurityException;

  /**
   * Returns the {@link PresenceProxy} of a given {@link AgentIdentifier}.
   *
   * @param caller an {@link Agent} instance.
   * @param aid an {@link AgentIdentifier} instance.
   * @return a {@link PresenceProxy} instance.
   * @throws SecurityException    if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public PresenceProxy getRegistered(Agent caller, AgentIdentifier aid) throws SecurityException;

  /**
   * Tests if a given {@link AgentIdentifier} is available.
   *
   * @param caller an {@link Agent} instance.
   * @param aid an {@link AgentIdentifier} instance.
   * @return true if available, false otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public boolean isAvailable(Agent caller, AgentIdentifier aid) throws SecurityException;

  /**
   * Tests if a given {@link AgentIdentifier} is ad-available.
   *
   * @param caller an {@link Agent} instance.
   * @param aid an {@link AgentIdentifier} instance.
   * @return true if ad-available, false otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public boolean isAdvertisedAvailable(Agent caller, AgentIdentifier aid)
      throws SecurityException;

  /**
   * Tests if a given {@link Presence} is registered.
   *
   * @param caller an {@link Agent} instance.
   * @param p a {@link Presence} instance.
   * @return true if registered, false otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public boolean isRegistered(Agent caller, Presence p)
      throws SecurityException;

  /**
   * Adds a {@link PresenceRegistrationListener} that will be notified
   * for unregisters.
   *
   * @param caller an {@link Agent} instance.
   * @param prl a {@link PresenceRegistrationListener}.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
  public void addPresenceRegistrationListener(Agent caller,PresenceRegistrationListener prl)
      throws SecurityException;

  /**
   *
   * @param caller caller an {@link Agent} instance.
   * @param prl {@link PresenceRegistrationListener}.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                              the corresponding action.
   */
   public void removePresenceRegistrationListener(Agent caller,PresenceRegistrationListener prl)
      throws SecurityException;

}//interface PresenceService
